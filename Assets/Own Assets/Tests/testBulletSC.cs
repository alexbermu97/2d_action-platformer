﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class testBulletSC : MonoBehaviour
{
    private Rigidbody2D rb;
    private GameObject player;

    [SerializeField]
    private ParticleSystem impact;

    [SerializeField]
    private float healthDamage;
    [SerializeField]
    private float shieldDamage;
    [SerializeField]
    private float minDam;
    [SerializeField]
    private float maxDam;
    public float speed;

    [SerializeField]
    private AudioSource shieldSFX;
    [SerializeField]
    private AudioSource[] bulletFireSFX;
    [SerializeField]
    private AudioSource[] bulletHitSFX;

    private int cont = 1;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        player = GameObject.FindGameObjectWithTag("Player");
        shieldDamage = Random.Range(minDam, maxDam);
        bulletFireSFX[Random.Range(0, 3)].Play();

        rb.velocity = transform.right * speed;

        Destroy(gameObject, 3f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag != "LevelBound" && 
            collision.gameObject.tag != "Enemy" &&
            collision.gameObject.tag != "EnemyDetector")
        {
            if (collision.gameObject.tag == "PlayerShield" && cont == 1)
            {
                shieldSFX.Play();
                player.GetComponent<PlayerMana>().stopRegain = false;
                player.GetComponent<PlayerMana>().currentMP -= shieldDamage;
            }
            if (collision.gameObject == player && cont == 1)
            {
                player.GetComponent<PlayerHealth>().gotHit = true;
                player.GetComponent<PlayerHealth>().currentHP -= healthDamage;
            }
            bulletHitSFX[Random.Range(0, 3)].Play();
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
            gameObject.GetComponent<CapsuleCollider2D>().enabled = false;
            rb.velocity = transform.right * 0;
            if (impact.isStopped)
                impact.Play();
            cont++;
            Destroy(gameObject, 4f);
        }
    }
}