﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyTestShoot : MonoBehaviour
{
    [SerializeField]
    private GameObject bullet;
    [SerializeField]
    private Transform bulletSpawn;

    [SerializeField]
    private float fireRate;
    [SerializeField]
    private float nextShot;

    void Start()
    {
        transform.Rotate(0, 180, 0);
    }

    void Update()
    {
        if (nextShot < Time.time)
        {
            nextShot = fireRate + Time.time;
            Instantiate(bullet, bulletSpawn.position, bulletSpawn.rotation);
        }
    }
}
