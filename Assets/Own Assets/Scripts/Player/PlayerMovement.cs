﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMovement : MonoBehaviour
{
    private Rigidbody2D rb;
    private PlayerShield shield;
    private PlayerMelee melee;
    private PlayerHealth health;

    //Movimiento
    [SerializeField]
    [Range(0f, 25f)]
    private float movSp;
    private float saveSp;
    private float waterSp;
    [SerializeField]
    [Tooltip("Usar para ajustar mejor la velocidad de movimiento.")]
    [Range(0f, 5f)]
    private float multiplier;
    [HideInInspector]
    public float h;

    [Range(0, 1)]
    [Tooltip("Lo mucho que hay que mover el joystick para que el personaje empiece a moverse.")]
    public float deadZone;

    private bool lookRight;

    public bool canMove;
    public bool onIce;
    public bool inWater;

    //Joystick virtual
    [SerializeField]
    private FloatingJoystick j;

    [HideInInspector]
    public bool playIdle2;
    public float timeToPlayIdle2;

    //Sonidos
    [SerializeField]
    private AudioSource[] stepSFX;
    [SerializeField]
    private AudioSource[] waterStepSFX;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        shield = GetComponent<PlayerShield>();
        melee = GetComponent<PlayerMelee>();
        health = GetComponent<PlayerHealth>();

        saveSp = movSp;
        waterSp = movSp * 0.65f;

        j = GameObject.Find("Floating Joystick").GetComponent<FloatingJoystick>();

        lookRight = true;

        timeToPlayIdle2 = Random.Range(10, 30);
    }

    void Update()
    {
        if (shield.shieldUp || !melee.canAttack || health.isDead || health.gotHit)
            canMove = false;
        else if (!shield.shieldUp && melee.canAttack && !health.gotHit)
            canMove = true;

        if (inWater)
            movSp = waterSp;
        if (!inWater)
            movSp = saveSp;

        if (canMove)
        {
            h = j.Horizontal;

            if (h > 0.1f && !lookRight)
                TurnAround();
            if (h < -0.1f && lookRight)
                TurnAround();

            if (onIce)
            {
                if (h > -deadZone && h < deadZone)
                    rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y);
            }
            else
            {
                if (h > -deadZone && h < deadZone)
                    rb.velocity = new Vector2(0, rb.velocity.y);
            }
            if (h >= deadZone || h <= -deadZone)
                rb.velocity = new Vector2(h * movSp * multiplier, rb.velocity.y);

            if (h > -0.1f && h < 0.1f)
                timeToPlayIdle2 -= Time.deltaTime;
        }
        if (timeToPlayIdle2 <= 0)
        {
            playIdle2 = true;
            timeToPlayIdle2 = 0;
        }
    }

    void TurnAround()
    {
        lookRight = !lookRight;
        transform.Rotate(0f, 180f, 0f);
    }

    public void EndIdle2()
    {
        timeToPlayIdle2 = Random.Range(10, 30);
        playIdle2 = false;
    }

    public void StepSounds()
    {
        if (!inWater)
            stepSFX[Random.Range(0, 4)].Play();
        else
            waterStepSFX[Random.Range(0, 3)].Play();
    }
}