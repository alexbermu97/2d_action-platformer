﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneWayCollide : MonoBehaviour
{
    [SerializeField]
    private PlayerJump jump;
    [SerializeField]
    private float rayDistance;
    private RaycastHit2D hit1;
    private RaycastHit2D hit2;

    private void FixedUpdate()
    {
        hit1 = Physics2D.Raycast(new Vector2(transform.position.x + 0.32f, transform.position.y - 0.2f), 
            Vector2.down, rayDistance, LayerMask.NameToLayer("OneWay"));

        hit2 = Physics2D.Raycast(new Vector2(transform.position.x - 0.32f, transform.position.y - 0.2f), 
            Vector2.down, rayDistance, LayerMask.NameToLayer("OneWay"));

        Debug.DrawRay(new Vector2(transform.position.x + 0.32f, transform.position.y - 0.2f), 
            Vector2.down * rayDistance, Color.cyan);

        Debug.DrawRay(new Vector2(transform.position.x - 0.32f, transform.position.y - 0.2f), 
            Vector2.down * rayDistance, Color.cyan);

        if (hit1.collider != null || hit2.collider != null)
        {
            jump.onGround = true;
            jump.oneWayColliding = true;
            Debug.Log(hit1.collider.name);
            Debug.Log(hit2.collider.name);
        }
        if (hit1.collider == null && hit2.collider == null)
        {
            jump.onGround = false;
            jump.oneWayColliding = false;
        }
    }
}
