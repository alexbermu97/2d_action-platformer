﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMelee : MonoBehaviour
{
    //Botón táctil
    [SerializeField]
    private TouchButton tb;

    private PlayerMovement mov;

    [SerializeField]
    private GameObject meleeCollision;
    [SerializeField]
    private GameObject comboCollision;
    [SerializeField]
    private GameObject meleeFX;

    //[HideInInspector]
    public bool canAttack;
    //[HideInInspector]
    public bool attacked;
    [HideInInspector]
    public bool canCombo;
    [HideInInspector]
    public bool combo;

    [SerializeField]
    private int cont;

    //Sonidos
    [SerializeField]
    private AudioSource[] meleeSounds;

    void Start()
    {
        mov = GetComponent<PlayerMovement>();
        tb = GameObject.Find("Melee Button").GetComponent<TouchButton>();
        meleeCollision.GetComponent<BoxCollider2D>().enabled = false;
        comboCollision.GetComponent<BoxCollider2D>().enabled = false;
        cont = 0;
    }

    void Update()
    {
        Attack();
    }

    void Attack()
    {
        if (tb.pressed && cont == 0 && canAttack)
        {
            attacked = true;
            canAttack = false;
            cont++;
        }
        if (tb.pressed && cont == 1 && canCombo)
        {
            //Do combo
            print("combo");
            cont++;
            combo = true;
            canCombo = false;
        }
    }
    public void StartAttack()
    {
        meleeCollision.GetComponent<MeleeDamage>().cont = 1;
        comboCollision.GetComponent<MeleeDamage>().cont = 1;
    }
    public void RegisterHit()
    {
        meleeSounds[Random.Range(0, 3)].Play();
        //Se activa el registro de golpes y se activan las partículas
        meleeCollision.GetComponent<BoxCollider2D>().enabled = true;
        if (meleeFX.GetComponent<ParticleSystem>().isStopped)
            meleeFX.GetComponent<ParticleSystem>().Play();
    }
    public void EndRegisterHit()
    {
        meleeCollision.GetComponent<BoxCollider2D>().enabled = false;
        if (meleeFX.GetComponent<ParticleSystem>().isPlaying)
            meleeFX.GetComponent<ParticleSystem>().Stop();
    }
    public void ComboRegisterHit()
    {
        meleeSounds[Random.Range(0, 3)].Play();
        comboCollision.GetComponent<BoxCollider2D>().enabled = true;
        meleeCollision.GetComponent<BoxCollider2D>().enabled = false;
        if (meleeFX.GetComponent<ParticleSystem>().isStopped)
            meleeFX.GetComponent<ParticleSystem>().Play();
    }
    public void EndComboRegisterHit()
    {
        comboCollision.GetComponent<BoxCollider2D>().enabled = false;
        if (meleeFX.GetComponent<ParticleSystem>().isPlaying)
            meleeFX.GetComponent<ParticleSystem>().Stop();
    }
    public void CanAttack()
    {
        //Permite volver a atacar
        canAttack = true;
    }
    public void EndAttack()
    {
        attacked = false;
        cont = 0;
        combo = false;
        meleeCollision.GetComponent<BoxCollider2D>().enabled = false;
        comboCollision.GetComponent<BoxCollider2D>().enabled = false;
    }
    public void ComboWindow()
    {
        //En un punto de la animación se permite volver a atacar para hacer otro ataque más rápido
        canCombo = true;
    }
    public void EndComboWindow()
    {
        canCombo = false;
    }
}