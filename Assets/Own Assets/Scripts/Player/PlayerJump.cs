﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerJump : MonoBehaviour
{
    private Rigidbody2D rb;
    private PlayerShield shield;
    private PlayerMana mana;
    private PlayerHealth health;

    private GameObject manager;

    [SerializeField]
    private GameObject coli;

    //Botón táctil
    [SerializeField]
    private TouchButton tb;

    //Salto
    [SerializeField]
    [Range(0f, 50f)]
    private float jumpForce;
    [SerializeField]
    [Range(0f, 50f)]
    private float jumpForceShort;
    //[HideInInspector]
    public bool onGround;
    [HideInInspector]
    public bool jumped;
    [HideInInspector]
    public bool canJump;
    [HideInInspector]
    public bool fall;
    [HideInInspector]
    public bool cancelJump;

    public bool oneWayColliding;

    //Jet
    public float manaConsumption; //Puede ser modificado en el futuro por otros scripts
    [SerializeField]
    private float jetForce;
    [HideInInspector]
    public bool jetIsOn;

    //Joystick virtual
    [SerializeField]
    private FloatingJoystick j;
    private float v;

    //Sonidos
    [SerializeField]
    private AudioSource[] jumpSFX;

    void Start()
    {
        manager = GameObject.Find("GameManager");

        if (manager.GetComponent<UpgradesManager>().jetUpgrade1 != 0)
            jetForce += 1;
        if (manager.GetComponent<UpgradesManager>().jetUpgrade2 != 0)
            jetForce += 1;
        if (manager.GetComponent<UpgradesManager>().jetUpgrade3 != 0)
            jetForce += 1;
        if (manager.GetComponent<UpgradesManager>().jetUpgrade4 != 0)
            jetForce += 1;
        if (manager.GetComponent<UpgradesManager>().jetUpgrade5 != 0)
            jetForce += 1;

        rb = GetComponent<Rigidbody2D>();
        shield = GetComponent<PlayerShield>();
        mana = GetComponent<PlayerMana>();
        health = GetComponent<PlayerHealth>();

        tb = GameObject.Find("Jump Button").GetComponent<TouchButton>();
        j = GameObject.Find("Floating Joystick").GetComponent<FloatingJoystick>();
    }

    void Update()
    {
        if (health.gotHit)
            canJump = false;
        else
            canJump = true;
        v = j.Vertical;
        if (onGround || oneWayColliding)
        {
            jumped = false;
            fall = false;
            jetIsOn = false;
        }
    }

    void FixedUpdate()
    {
        if (!shield.shieldUp && !health.isDead)
        {
            if (v > -0.5f)
            {
                NormalJump();
                JetPack();
            }
            Drop();
        }
    }

    void NormalJump()
    {
        if (tb.pressed && !jumped && onGround && canJump)
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpForce);
            onGround = false;
            oneWayColliding = false;
            jumped = true;
        }
        if (!tb.pressed)
        {
            if (!onGround && !oneWayColliding)
            {
                cancelJump = true;
            }
        }

        if (cancelJump)
        {
            if (rb.velocity.y > jumpForceShort)
                rb.velocity = new Vector2(rb.velocity.x, jumpForceShort);
            cancelJump = false;
            fall = true;
        }
    }

    void JetPack()
    {
        if (!onGround && !oneWayColliding && tb.longPressed && mana.currentMP > 0 && canJump)
        {
            fall = false;
            jetIsOn = true;
            rb.velocity = new Vector2(rb.velocity.x, jetForce);
            mana.currentMP -= manaConsumption * Time.deltaTime;
            //Animación de jet
        }
        if (!tb.pressed && jetIsOn)
        {
            if (!onGround && !oneWayColliding)
                fall = true;
        }
    }

    void Drop()
    {
        GameObject[] oneWays = GameObject.FindGameObjectsWithTag("OneWayPlatform");
        if (v <= -0.5f && oneWayColliding)
        {
            coli.GetComponent<GroundCollide>().enabled = false;
            foreach (GameObject oneWay in oneWays)
            {
                oneWay.GetComponent<PlatformEffector2D>().rotationalOffset = 180f;
            }
        }
        else
        {
            coli.GetComponent<GroundCollide>().enabled = true;
            foreach (GameObject oneWay in oneWays)
            {
                oneWay.GetComponent<PlatformEffector2D>().rotationalOffset = 0f;
            }
        }
    }

    public void PlayJumpSound()
    {
        jumpSFX[Random.Range(0, 3)].Play();
    }
}