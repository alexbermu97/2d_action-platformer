﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundCollide : MonoBehaviour
{
    [SerializeField]
    private PlayerJump jump;
    [SerializeField]
    private PlayerMovement move;
    [SerializeField]
    private float rayDistance;
    private RaycastHit2D hit1;
    private RaycastHit2D hit2;

    private void FixedUpdate()
    {
        //Rayos
        hit1 = Physics2D.Raycast(new Vector2(transform.position.x + 0.4f, transform.position.y - 0.15f), 
            Vector2.down, rayDistance, 9 << LayerMask.NameToLayer("GroundCollisions"));

        hit2 = Physics2D.Raycast(new Vector2(transform.position.x - 0.4f, transform.position.y - 0.15f), 
            Vector2.down, rayDistance, 9 << LayerMask.NameToLayer("GroundCollisions"));

        //Representación visual de los rayos
        Debug.DrawRay(new Vector2(transform.position.x + 0.4f, transform.position.y - 0.15f), 
            Vector2.down * rayDistance, Color.green);

        Debug.DrawRay(new Vector2(transform.position.x - 0.4f, transform.position.y - 0.15f), 
            Vector2.down * rayDistance, Color.green);

        //Acciones
        if (hit1.collider != null || hit2.collider != null)
        {
            jump.onGround = true;

            if (hit1.collider.tag == "IcePlatform" || hit2.collider.tag == "IcePlatform")
                move.onIce = true;
            else if (hit1.collider.tag != "IcePlatform" && hit2.collider.tag != "IcePlatform")
                move.onIce = false;

            if (hit1.collider.tag == "OneWayPlatform" || hit2.collider.tag == "OneWayPlatform")
                jump.oneWayColliding = true;
            else if (hit1.collider.tag != "OneWayPlatform" && hit2.collider.tag != "OneWayPlatform")
                jump.oneWayColliding = false;
        }
        if (hit1.collider == null && hit2.collider == null)
        {
            jump.onGround = false;
            jump.oneWayColliding = false;
        }
    }
}
