﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class DetectEnemies : MonoBehaviour
{
    private RaycastHit2D rayHit;
    [SerializeField]
    private PlayerShoot pShoot;
    [SerializeField]
    private Transform bulletSpawn;
    [SerializeField]
    private List<GameObject> enemies = new List<GameObject>();
    [SerializeField]
    private GameObject[] durr;
    private float distance;
    [HideInInspector]
    public Vector3 direction;

    [SerializeField]
    private float enemyDistance;

    private void Update()
    {
        UpdateList();
        Ray();
    }

    void Ray()
    {
        foreach (GameObject enemy in enemies)
        {
            rayHit = Physics2D.Raycast(bulletSpawn.position, enemy.transform.position);
            Debug.DrawLine(bulletSpawn.position, enemy.transform.position, Color.green); 
            distance = Vector2.Distance(bulletSpawn.position, enemy.transform.position);

            if (distance <= enemyDistance)
            {
                if (!enemy.GetComponent<EnemyAir>().isDead)
                {
                    direction = (enemy.transform.position - bulletSpawn.position).normalized;
                    pShoot.enemyInRange = true;
                }
            }
        }
    }

    public void UpdateList()
    {
        durr = GameObject.FindGameObjectsWithTag("Enemy");
        enemies = durr.ToList();
    }
    /*public void RemoveFromList(GameObject dude)
    {
        enemies.Remove(dude);
        durr = enemies.ToArray();
    }*/
}