﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    private PlayerMana mana;
    [SerializeField]
    private float manaConsumption;
    [SerializeField]
    private float smallShotConsumption;
    [SerializeField]
    private DetectEnemies detEn;

    private PlayerHealth health;

    //[HideInInspector]
    public bool enemyInRange;

    //Proyectiles
    [SerializeField]
    private GameObject smallProjectile;
    [SerializeField]
    private GameObject bigProjectile;
    [SerializeField]
    private Transform projectileSpawn;

    [SerializeField]
    private GameObject parts1;
    [SerializeField]
    private GameObject parts2;

    //Botón táctil
    [SerializeField]
    private TouchButtonAttack tb;

    //Tipos de disparo
    [HideInInspector]
    [Tooltip("1->Semi, 2->Auto")]
    public int shotType = 1; //Modificado por el árbol de habilidades

    [HideInInspector]
    public bool bigShot = false;
    [HideInInspector]
    public bool shooting;

    private int cont;

    void Start()
    {
        mana = GetComponent<PlayerMana>();
        health = GetComponent<PlayerHealth>();
        cont = 1;

        tb = GameObject.Find("Attack Button").GetComponent<TouchButtonAttack>();

        parts1.GetComponent<ParticleSystem>().Stop(); 
        parts2.GetComponent<ParticleSystem>().Stop();
    }

    void Update()
    {
        parts1.transform.position = new Vector3(parts1.transform.position.x, parts1.transform.position.y, -0.1f);
        parts2.transform.position = new Vector3(parts1.transform.position.x, parts1.transform.position.y, -0.1f);

        if (!health.isDead)
        {
            if (shotType == 1)
                ProjectileShots();
            if (shotType == 2)
                AutoFire();
        }
    }

    void ProjectileShots()
    {
        if (tb.tapped)
            shooting = true;
        if (tb.released)
            shooting = false;
        if (tb.winding)
        {
            mana.stopRegain = true;
            if (parts1.GetComponent<ParticleSystem>().isStopped && parts2.GetComponent<ParticleSystem>().isStopped)
            {
                parts1.GetComponent<ParticleSystem>().Play();
                parts2.GetComponent<ParticleSystem>().Play();
            }
        }
        if (tb.pressed && cont == 1)
        {
            cont++;
            if (smallShotConsumption <= mana.currentMP)
            {
                if (!enemyInRange)
                    Instantiate(smallProjectile, projectileSpawn.position, projectileSpawn.rotation);
                else
                {
                    float rotation = Mathf.Atan2(detEn.direction.y, detEn.direction.x) * Mathf.Rad2Deg;
                    Instantiate(smallProjectile, projectileSpawn.position, Quaternion.Euler(0f, 0f, rotation));
                }
                mana.currentMP -= smallShotConsumption;
            }
            if (parts1.GetComponent<ParticleSystem>().isPlaying && parts2.GetComponent<ParticleSystem>().isPlaying)
            {
                parts1.GetComponent<ParticleSystem>().Stop();
                parts2.GetComponent<ParticleSystem>().Stop();
            }
            tb.pressed = false;
        }
        if (tb.longPressed && cont == 1)
        {
            bigShot = true;
            cont++;
            if (manaConsumption <= mana.currentMP)
            {
                if (!enemyInRange)
                    Instantiate(bigProjectile, projectileSpawn.position, projectileSpawn.rotation);
                else
                {
                    float rotation = Mathf.Atan2(detEn.direction.y, detEn.direction.x) * Mathf.Rad2Deg;
                    Instantiate(bigProjectile, projectileSpawn.position, Quaternion.Euler(0f, 0f, rotation));
                }
                mana.currentMP -= manaConsumption;
            }
            if (parts1.GetComponent<ParticleSystem>().isPlaying && parts2.GetComponent<ParticleSystem>().isPlaying)
            {
                parts1.GetComponent<ParticleSystem>().Stop();
                parts2.GetComponent<ParticleSystem>().Stop();
            }
            tb.longPressed = false;
            shooting = false;
        }
        if (!tb.pressed || !tb.longPressed)
        {
            cont = 1;
            bigShot = false;
        }
    }

    void AutoFire()
    {
        if (tb.tapped)
            Instantiate(smallProjectile, projectileSpawn.position, projectileSpawn.rotation);
    }
}