﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShield : MonoBehaviour
{
    [HideInInspector]
    public bool shieldUp;
    private PlayerMana mana;
    private PlayerHealth health;

    //Botón táctil
    [SerializeField]
    private TouchButton tb;

    //Escudo
    [SerializeField]
    private GameObject shieldObj;

    void Start()
    {
        health = GetComponent<PlayerHealth>();
        tb = GameObject.Find("Shield Button").GetComponent<TouchButton>();
        mana = GetComponent<PlayerMana>();
        shieldUp = false;
        shieldObj.SetActive(false);
    }

    void Update()
    {
        if (!health.isDead)
        {
            if (tb.tapped && mana.currentMP > 0)
            {
                shieldUp = true;
                shieldObj.SetActive(true);
            }
            else
            {
                shieldUp = false;
                shieldObj.SetActive(false);
            }
            shieldObj.transform.position = new Vector3(shieldObj.transform.position.x, shieldObj.transform.position.y, -2f);
            ChangeColor();
        }
    }

    void ChangeColor()
    {
        shieldObj.GetComponent<SpriteRenderer>().color = Color.Lerp(Color.red, Color.blue, mana.currentMP/mana.totalMP);
    }
}