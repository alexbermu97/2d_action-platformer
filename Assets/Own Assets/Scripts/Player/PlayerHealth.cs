﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour
{
    [SerializeField]
    private Slider healthBar;

    private GameObject manager;

    [SerializeField]
    private float totalHP = 5;
    //[HideInInspector]
    public float currentHP;
    public bool isDead;

    [HideInInspector]
    public bool gotHit;
    [SerializeField]
    private BoxCollider2D box;
    [SerializeField]
    [Tooltip("Tiempo de invencibilidad desde que el jugador recibe daño.")]
    private float timeToRecover;
    private float cont = 0;
    private PlayerMelee melee;
    public float timeToRespawn = 0;

    public bool inTutorial;

    //Control de checpoint
    public Transform currentCheck;

    //Sonidos
    [SerializeField]
    private AudioSource[] hitSounds;
    [SerializeField]
    private AudioSource deathSound;

    void Start()
    {
        manager = GameObject.Find("GameManager");
        melee = GetComponent<PlayerMelee>();
        if (manager.GetComponent<UpgradesManager>().HPUpgrade1 != 0)
            totalHP += 1;
        if (manager.GetComponent<UpgradesManager>().HPUpgrade2 != 0)
            totalHP += 1;
        if (manager.GetComponent<UpgradesManager>().HPUpgrade3 != 0)
            totalHP += 1;
        if (manager.GetComponent<UpgradesManager>().HPUpgrade4 != 0)
            totalHP += 1;
        if (manager.GetComponent<UpgradesManager>().HPUpgrade5 != 0)
            totalHP += 1;

        healthBar = GameObject.Find("Health Bar").GetComponent<Slider>();
        healthBar.value = totalHP / totalHP; //Esto es para que el resultado siempre de 1 y rellene la barra correctamente
        currentHP = totalHP;
    }

    void Update()
    {
        healthBar.value = currentHP / totalHP; //Para que el valor quede por debajo de 1
        if (currentHP > totalHP)
            currentHP = totalHP;
        if (gotHit)
            Hit();
        if (currentHP <= 0)
            Dead();
        if (!isDead)
            StopCoroutine(LoadCheckpointTutorial());
    }

    private void Hit()
    {
        melee.canAttack = false;
        box.enabled = false;
        cont += Time.deltaTime;
        if (cont >= timeToRecover)
        {
            box.enabled = true;
            gotHit = false;
            melee.canAttack = true;
            cont = 0;
        }
    }

    void Dead()
    {
        //Secuencia de muerte del jugador
        //Animación de muerte
        isDead = true;
        if (inTutorial && isDead)
        {
            //Cargar el checkpoint en el tutorial
            timeToRespawn += Time.deltaTime;
            if (timeToRespawn >= 4f)
                RespawnTutorial();
        }
        else
        {
            //Pasar a pantalla de misión fallida
            StartCoroutine(GoToFailScreen());
        }
    }

    IEnumerator GoToFailScreen()
    {
        yield return new WaitForSeconds(4f);
        SceneManager.LoadScene("Failed");
    }
    IEnumerator LoadCheckpointTutorial()
    {
        yield return new WaitForSeconds(4f);
        currentHP = totalHP;
        gameObject.transform.position = currentCheck.position;
        isDead = false;
    }
    void RespawnTutorial()
    {
        currentHP = totalHP;
        gameObject.transform.position = currentCheck.position;
        timeToRespawn = 0;
        isDead = false;
    }

    public void PlayDeathSound()
    {
        deathSound.Play();
    }

    public void PlayHitSound()
    {
        hitSounds[Random.Range(0, 3)].Play();
    }
}