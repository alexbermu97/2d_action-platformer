﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMana : MonoBehaviour
{
    [SerializeField]
    private Slider manaBar;

    private GameObject manager;

    private PlayerShield shield;
    private PlayerJump jump;
    private PlayerShoot shoot;

    public float totalMP = 100;
    //[HideInInspector]
    public float currentMP;

    //Velocidad a la que se recupera maná
    //Debe poder ser modificada por otros scripts
    public float recoverySp;
    public float timeToStartRecovery;

    //Otros scripts deben poder acceder a estas variables para resetearlas
    [HideInInspector]
    public bool stopRegain;
    private bool startRegain;

    private float cont = 0;

    void Start()
    {
        manager = GameObject.Find("GameManager");

        if (manager.GetComponent<UpgradesManager>().MPUpgrade1 != 0)
            totalMP += 25;
        if (manager.GetComponent<UpgradesManager>().MPUpgrade2 != 0)
            totalMP += 25;
        if (manager.GetComponent<UpgradesManager>().MPUpgrade3 != 0)
            totalMP += 25;
        if (manager.GetComponent<UpgradesManager>().MPUpgrade4 != 0)
            totalMP += 25;

        if (manager.GetComponent<UpgradesManager>().regenUpgrade1 != 0)
            recoverySp += 2;
        if (manager.GetComponent<UpgradesManager>().regenUpgrade2 != 0)
            recoverySp += 2;
        if (manager.GetComponent<UpgradesManager>().regenUpgrade3 != 0)
            recoverySp += 2;
        if (manager.GetComponent<UpgradesManager>().regenUpgrade4 != 0)
            recoverySp += 2;
        if (manager.GetComponent<UpgradesManager>().regenUpgrade5 != 0)
            recoverySp += 2;

        manaBar = GameObject.Find("Mana Bar").GetComponent<Slider>();
        currentMP = totalMP;
        jump = GetComponent<PlayerJump>();
        shield = GetComponent<PlayerShield>();
        shoot = GetComponent<PlayerShoot>();
    }

    void Update()
    {
        if (currentMP < totalMP && !stopRegain)
        {
            cont += Time.deltaTime;
            if (cont >= timeToStartRecovery)
                startRegain = true;
        }
        if (stopRegain)
        {
            cont = 0;
            startRegain = false;
        }
        if (startRegain)
            currentMP += recoverySp * Time.deltaTime;

        if (currentMP != totalMP)
        {
            if (shield.shieldUp || jump.jetIsOn || shoot.bigShot)
                stopRegain = true;
            else
                stopRegain = false;
        }
        if (currentMP >= totalMP)
        {
            currentMP = totalMP;
            stopRegain = true;
        }
        if (currentMP < 0)
            currentMP = 0;

        manaBar.value = currentMP / totalMP;
    }
}