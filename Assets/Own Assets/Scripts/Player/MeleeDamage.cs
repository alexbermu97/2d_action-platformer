﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeDamage : MonoBehaviour
{
    [SerializeField]
    private float damageToHP;

    public int cont = 1;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag != "LevelBound" && collision.gameObject.tag != "Player" && collision.gameObject.tag != "PlayerBullet")
        {
            if (collision.gameObject.tag == "Enemy" && cont == 1)
            {
                cont++;
                collision.gameObject.GetComponent<EnemyAir>().TakeDamage(damageToHP);
            }
        }
    }
}