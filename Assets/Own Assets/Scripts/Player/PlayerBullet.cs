﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBullet : MonoBehaviour
{
    private Rigidbody2D rb;

    [SerializeField]
    private ParticleSystem impact;

    [SerializeField]
    private float damageToHP;
    [SerializeField]
    private float speed;
    [SerializeField]
    private float timeAlive;
    [SerializeField]
    private AudioSource[] bulletFireSFX;
    [SerializeField]
    private AudioSource[] bulletHitSFX;

    private int cont = 1;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        transform.position = new Vector3(transform.position.x, transform.position.y, -0.2f);
        rb.velocity = transform.right * speed;
        Destroy(gameObject, timeAlive);
        bulletFireSFX[Random.Range(0, 3)].Play();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag != "LevelBound" && 
            collision.gameObject.tag != "Player" && 
            collision.gameObject.tag != "PlayerBullet" &&
            collision.gameObject.tag != "EnemyDetector")
        {
            if (collision.gameObject.tag == "Enemy" && cont == 1)
            {
                if (collision.gameObject.GetComponent<EnemyAir>())
                    collision.gameObject.GetComponent<EnemyAir>().TakeDamage(damageToHP);
                else if (collision.gameObject.GetComponent<EnemyBoss>())
                    collision.gameObject.GetComponent<EnemyBoss>().TakeDamage(damageToHP);
            }
            bulletHitSFX[Random.Range(0, 3)].Play();
            cont++;
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
            gameObject.GetComponent<CapsuleCollider2D>().enabled = false;
            rb.velocity = transform.right * 0;
            if (impact.isStopped)
                impact.Play();
            Destroy(gameObject, 4f);
        }
    }
}