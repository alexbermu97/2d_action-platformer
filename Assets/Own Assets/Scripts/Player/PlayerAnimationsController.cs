﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationsController : MonoBehaviour
{
    private Animator anim;
    private PlayerShield shield;
    private PlayerMovement move;
    private PlayerJump jump;
    private PlayerMelee melee;
    private PlayerHealth health;

    void Start()
    {
        anim = GetComponent<Animator>();
        shield = GetComponent<PlayerShield>();
        move = GetComponent<PlayerMovement>();
        jump = GetComponent<PlayerJump>();
        melee = GetComponent<PlayerMelee>();
        health = GetComponent<PlayerHealth>();
    }

    void Update()
    {
        if (health.isDead)
        {
            anim.SetBool("Dead", true);
            anim.SetBool("ShieldOn", false);
            anim.SetBool("Idle", false);
            anim.SetBool("Running", false);
            anim.SetBool("Jumping", false);
            anim.SetBool("Falling", false);
            anim.SetBool("Melee", false);
        }
        //Todas las animaciones pueden ejecutarse mientras el jugador esté vivo
        if (!health.isDead)
        {
            anim.SetBool("Dead", false);
            //Si el escudo está subido, todas las animaciones se detienen y se ejecuta la de escudo
            if (shield.shieldUp)
            {
                anim.SetBool("ShieldOn", true);
                anim.SetBool("Idle", false);
                anim.SetBool("Running", false);
                anim.SetBool("Jumping", false);
                anim.SetBool("Falling", false);
                anim.SetBool("Melee", false);
            }
            //Si el escudo está bajado, se para la animación de escudo y se ejecutan las demás
            if (!shield.shieldUp)
            {
                anim.SetBool("ShieldOn", false);
                if (health.gotHit)
                    anim.SetBool("GotHit", true);
                if (!health.gotHit)
                    anim.SetBool("GotHit", false);
                //Si se salta se cancelan todas las animaciones salvo salto
                if (jump.jumped)
                {
                    anim.SetBool("Jumping", true);
                    anim.SetBool("Idle", false);
                    anim.SetBool("Running", false);
                    anim.SetBool("Falling", false);

                    /*if (melee.attacked)
                        anim.SetBool("Melee", true);
                    if (!melee.attacked)
                        anim.SetBool("Melee", false);*/
                }
                //Si ya no se está saltando y no se ha tocado el suelo se reproduce la animación de caída
                if (jump.fall)
                {
                    anim.SetBool("Idle", false);
                    anim.SetBool("Running", false);
                    anim.SetBool("Jumping", false);
                    anim.SetBool("Falling", true);

                    /*if (melee.attacked)
                        anim.SetBool("Melee", true);
                    if (!melee.attacked)
                        anim.SetBool("Melee", false);*/
                }
                //Si se está en el suelo se cancelan las animaciones de salto y caída y se ejecutan las de movimiento
                if (jump.onGround && !jump.jumped)
                {
                    anim.SetBool("Jumping", false);
                    anim.SetBool("Falling", false);
                    if (move.h > -move.deadZone && move.h < move.deadZone)
                    {
                        anim.SetBool("Idle", true);
                        anim.SetBool("Running", false);
                        if (move.playIdle2)
                            anim.SetBool("Idle2", true);
                        if (!move.playIdle2)
                            anim.SetBool("Idle2", false); 
                        
                        /*if (melee.attacked)
                        {
                            anim.SetBool("Melee", true);
                            if (melee.combo)
                                anim.SetBool("Combo", true);
                        }
                        if (!melee.attacked)
                            anim.SetBool("Melee", false);
                        if (!melee.combo)
                            anim.SetBool("Combo", false);*/
                    }
                    if (move.h >= move.deadZone || move.h <= -move.deadZone)
                    {
                        anim.SetBool("Idle", false);
                        move.playIdle2 = false;
                        anim.SetBool("Idle2", false);
                        anim.SetBool("Running", true);

                        /*if (melee.attacked)
                            anim.SetBool("Melee", true);
                        if (!melee.attacked)
                            anim.SetBool("Melee", false);*/
                    }
                }
                if (melee.attacked)
                {
                    anim.SetBool("Melee", true);
                    if (melee.combo)
                        anim.SetBool("Combo", true);
                }
                if (!melee.attacked)
                    anim.SetBool("Melee", false);
                if (!melee.combo)
                    anim.SetBool("Combo", false);
            }
        }
    }
}