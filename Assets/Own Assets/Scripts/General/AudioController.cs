﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    private GameObject manager;
    private AudioSource audioS;

    [SerializeField]
    private bool music;
    [SerializeField]
    private bool sound;

    void Start()
    {
        manager = GameObject.Find("GameManager");
        audioS = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (music)
        {
            if (manager.GetComponent<SoundManager>().music != 0)
                audioS.enabled = true;
            else
                audioS.enabled = false;
        }
        if (sound)
        {
            if (manager.GetComponent<SoundManager>().sound != 0)
                audioS.enabled = true;
            else
                audioS.enabled = false;
        }
    }
}