﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour
{
    public GameObject[] enemies;

    public GameObject exit;
    private bool amActive;

    [HideInInspector]
    public int chosenEnemy;
    [HideInInspector]
    public int cont = 0;

    public virtual void Start()
    {
        exit = GameObject.FindGameObjectWithTag("LevelExit");

        if (cont == 0)
        {
            chosenEnemy = Random.Range(0, enemies.Length);
            Instantiate(enemies[chosenEnemy], gameObject.transform.position, gameObject.transform.rotation);
            cont++;
        }
        if (exit.activeSelf)
            amActive = true;
        else
            amActive = false;
    }

    public virtual void Update()
    {
        if (amActive)
        {
            if (exit.GetComponent<ExitLevel>().hasArtifact && cont == 1)
            {
                chosenEnemy = Random.Range(0, enemies.Length);
                Instantiate(enemies[chosenEnemy], gameObject.transform.position, gameObject.transform.rotation);
                cont++;
            }
        }
    }
}