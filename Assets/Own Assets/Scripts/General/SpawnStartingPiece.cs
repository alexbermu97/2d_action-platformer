﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnStartingPiece : MonoBehaviour
{
    [SerializeField]
    private GameObject[] startPieces;
    private int chosenStartPiece;

    void Start()
    {
        chosenStartPiece = Random.Range(0, startPieces.Length);
        Instantiate(startPieces[chosenStartPiece], gameObject.transform.position, gameObject.transform.rotation);
    }
}
