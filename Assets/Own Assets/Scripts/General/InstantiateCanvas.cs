﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateCanvas : MonoBehaviour
{
    [SerializeField]
    private GameObject canvas;
    [SerializeField]
    private GameObject evSystem;

    void Start()
    {
        Instantiate(canvas);
        Instantiate(evSystem);
    }
}
