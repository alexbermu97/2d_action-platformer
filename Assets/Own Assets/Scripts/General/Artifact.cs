﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Artifact : MonoBehaviour
{
    private GameObject player;
    private GameObject artiImg;
    private GameObject exit;
    private AudioSource SFX;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        artiImg = GameObject.Find("ArtiImg");
        exit = GameObject.FindGameObjectWithTag("LevelExit");
        SFX = GameObject.Find("ArtifactPickUpSound").GetComponent<AudioSource>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == player.gameObject)
        {
            SFX.Play();
            exit.GetComponent<ExitLevel>().hasArtifact = true;
            artiImg.SetActive(true);
            Destroy(gameObject);
        }
    }
}
