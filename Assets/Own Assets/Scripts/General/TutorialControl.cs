﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TutorialControl : MonoBehaviour
{
    //Este script controla los eventos del tutorial

    private GameObject manager;

    //Colisiones externas
    [SerializeField]
    private GameObject checkTrigger;
    [SerializeField]
    private GameObject endTrigger;
    [SerializeField]
    private GameObject jumpTrigger;
    [SerializeField]
    private GameObject jetTrigger;
    [SerializeField]
    private GameObject waterTrigger;
    [SerializeField]
    private GameObject oneWayTrigger;
    [SerializeField]
    private GameObject oneWayDownTrigger;
    [SerializeField]
    private GameObject iceTrigger;
    [SerializeField]
    private GameObject shieldTrigger;

    //Tutoriales
    [SerializeField]
    private GameObject jumpTut;
    [SerializeField]
    private GameObject jetTut;
    [SerializeField]
    private GameObject waterTut;
    [SerializeField]
    private GameObject oneWayTut;
    [SerializeField]
    private GameObject oneWayDownTut;
    [SerializeField]
    private GameObject iceTut;
    [SerializeField]
    private GameObject shieldTut;
    [SerializeField]
    private GameObject attackTut;

    //Botones
    [SerializeField]
    private GameObject shieldB;
    private bool shieldBOff = true;
    [SerializeField]
    private GameObject meleeB;
    private bool meleeBOff = true;
    [SerializeField]
    private GameObject shootB;
    private bool shootBOff = true;
    [SerializeField]
    private GameObject jumpB;
    private bool jumpBOff = true;

    [SerializeField]
    private GameObject joy;
    [SerializeField]
    private GameObject player;
    [SerializeField]
    private float waitTime;
    [SerializeField]
    private GameObject levelRewards;
    private int isFirst;

    void Start()
    {
        manager = GameObject.Find("GameManager");
        manager.GetComponent<MoneyManager>().levelMoney = 0;

        isFirst = manager.GetComponent<TutorialManager>().firstTime;

        jumpTut.SetActive(false);
        jetTut.SetActive(false);
        waterTut.SetActive(false);
        oneWayTut.SetActive(false);
        oneWayDownTut.SetActive(false);
        iceTut.SetActive(false);
        shieldTut.SetActive(false);
        attackTut.SetActive(false);
    }

    void Update()
    {
        if(shieldBOff)
            shieldB.SetActive(false);
        else
            shieldB.SetActive(true);

        if (meleeBOff)
            meleeB.SetActive(false);
        else
            meleeB.SetActive(true);

        if (shootBOff)
            shootB.SetActive(false);
        else
            shootB.SetActive(true);

        if (jumpBOff)
            jumpB.SetActive(false);
        else
            jumpB.SetActive(true);
    }

    public void ActivateJumpButton()
    {
        jumpBOff = false;
        jumpTrigger.SetActive(false);
        jumpTut.SetActive(true);
        Time.timeScale = 0;
    }
    public void ActivateJetTutorial()
    {
        jetTrigger.SetActive(false);
        jetTut.SetActive(true);
        Time.timeScale = 0;
    }
    public void ActivateWaterTutorial()
    {
        waterTrigger.SetActive(false);
        waterTut.SetActive(true);
        Time.timeScale = 0;
    }
    public void ActivateOneWayTutorial()
    {
        oneWayTrigger.SetActive(false);
        oneWayTut.SetActive(true);
        Time.timeScale = 0;
    }
    public void ActivateOneWayDownTutorial()
    {
        oneWayDownTrigger.SetActive(false);
        oneWayDownTut.SetActive(true);
        Time.timeScale = 0;
    }
    public void ActivateIceTutorial()
    {
        iceTrigger.SetActive(false);
        iceTut.SetActive(true);
        Time.timeScale = 0;
    }
    public void ActivateShieldButton()
    {
        shieldBOff = false;
        shieldTrigger.SetActive(false);
        shieldTut.SetActive(true); 
        Time.timeScale = 0;
    }
    void ActivateAttackButtons()
    {
        meleeBOff = false;
        shootBOff = false;
        attackTut.SetActive(true);
        StopCoroutine(CountdownToAttackButtons());
        Time.timeScale = 0;
    }

    public void FinishTutorial()
    {
        //Cuando se llega al final del tutorial
        Time.timeScale = 1;
        levelRewards.GetComponent<LevelRewards>().gameEnded = true;
        if (isFirst == 0)
        {
            manager.GetComponent<TutorialManager>().firstTime = 1;
            PlayerPrefs.SetInt("Tutorial", manager.GetComponent<TutorialManager>().firstTime);
            SceneManager.LoadScene("RewardsMenu");
        }

        if (isFirst != 0)
        {
            levelRewards.GetComponent<LevelRewards>().gameReward = 0;
            SceneManager.LoadScene("LoadScreenPrep");
        }
    }

    public void SkipTutorial()
    {
        //Pulsando el botón se puede saltar el tutorial
        Time.timeScale = 1;
        levelRewards.GetComponent<LevelRewards>().gameEnded = true;
        if (isFirst == 0)
        {
            manager.GetComponent<TutorialManager>().firstTime = 1;
            PlayerPrefs.SetInt("Tutorial", manager.GetComponent<TutorialManager>().firstTime);
            SceneManager.LoadScene("RewardsMenu");
        }

        if (isFirst != 0)
        {
            levelRewards.GetComponent<LevelRewards>().gameReward = 0;
            SceneManager.LoadScene("LoadScreenPrep");
        }
    }

    public void Checkpoint()
    {
        player.GetComponent<PlayerHealth>().currentCheck = checkTrigger.transform;
        checkTrigger.GetComponent<TutorialTrigger>().enabled = false;
        checkTrigger.GetComponent<BoxCollider2D>().enabled = false;
    }

    public void CloseJumpTutorial()
    {
        jumpTut.SetActive(false);
        Time.timeScale = 1;
    }
    public void CloseJetTutorial()
    {
        jetTut.SetActive(false);
        Time.timeScale = 1;
    }
    public void CloseOneWayTutorial()
    {
        oneWayTut.SetActive(false);
        Time.timeScale = 1;
    }
    public void CloseOneWayDownTutorial()
    {
        oneWayDownTut.SetActive(false);
        Time.timeScale = 1;
    }
    public void CloseShieldTutorial()
    {
        shieldTut.SetActive(false);
        Time.timeScale = 1;
        StartCoroutine(CountdownToAttackButtons());
    }
    public void CloseAttackTutorial()
    {
        attackTut.SetActive(false);
        Time.timeScale = 1;
    }
    public void CloseWaterTutorial()
    {
        waterTut.SetActive(false);
        Time.timeScale = 1;
    }
    public void CloseIceTutorial()
    {
        iceTut.SetActive(false);
        Time.timeScale = 1;
    }

    IEnumerator CountdownToAttackButtons()
    {
        yield return new WaitForSeconds(waitTime);
        ActivateAttackButtons();
    }
}