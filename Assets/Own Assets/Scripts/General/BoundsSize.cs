﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundsSize : MonoBehaviour
{
    private PolygonCollider2D bounds;
    private GameObject lvlManager;
    [SerializeField]
    private int size;

    [SerializeField]
    private int point0X;
    [SerializeField]
    private int point0Y;
    [SerializeField]
    private int point1X;
    [SerializeField]
    private int point1Y;
    [SerializeField]
    private int point2Y;
    [SerializeField]
    private int point3Y;

    [SerializeField]
    [Tooltip("Multiplica el valor del tamaño del nivel")]
    private int mult;

    Vector2[] points;

    void Start()
    {
        bounds = GetComponent<PolygonCollider2D>();
        lvlManager = GameObject.Find("GameManager");
        size = lvlManager.GetComponent<LevelSizeManager>().levelSize + 1;

        points = bounds.points;
        points[0] = new Vector2(-point0X, -point0Y);
        points[1] = new Vector2(-point1X, point1Y);
        points[2] = new Vector2(size * mult, point2Y);
        points[3] = new Vector2(size * mult, -point3Y);
        bounds.points = points;
    }
}
