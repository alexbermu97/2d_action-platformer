﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTypeController : MonoBehaviour
{
    private GameObject manager;
    private GameObject arti;

    void Start()
    {
        manager = GameObject.Find("GameManager");
        arti = GameObject.Find("Arti");

        if (manager.GetComponent<GameType>().gameType == 1)
            arti.SetActive(false);
        if (manager.GetComponent<GameType>().gameType == 2)
            arti.SetActive(true);
    }
}