﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossSpawn : EnemySpawn
{
    public GameObject manager;

    public override void Start()
    {
        exit = GameObject.FindGameObjectWithTag("LevelExit");
        manager = GameObject.Find("GameManager");
        if (manager.GetComponent<GameType>().gameType == 1)
        {
            if (cont == 0)
            {
                chosenEnemy = Random.Range(0, enemies.Length);
                Instantiate(enemies[chosenEnemy], gameObject.transform.position, gameObject.transform.rotation);
                cont++;
            }
        }
    }
}