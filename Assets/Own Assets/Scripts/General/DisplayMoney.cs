﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DisplayMoney : MonoBehaviour
{
    private GameObject manager;
    [SerializeField]
    private TextMeshProUGUI counter;

    void Start()
    {
        manager = GameObject.Find("GameManager");
    }
    void Update()
    {
        counter.text = manager.GetComponent<MoneyManager>().myMoney.ToString() + (" Cr");
    }
}
