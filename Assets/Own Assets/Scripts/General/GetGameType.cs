﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetGameType : MonoBehaviour
{
    private GameObject manager;
    private int myGameType;
    private GameObject arti;
    private GameObject artiImg;
    private GameObject exitLevel;

    void Start()
    {
        exitLevel = GameObject.Find("Exit");
        arti = GameObject.Find("Arti");
        artiImg = GameObject.Find("ArtiImg");
        manager = GameObject.Find("GameManager");
        myGameType = manager.GetComponent<GameType>().gameType;

        artiImg.SetActive(false);

        if (myGameType == 1)
        {
            arti.SetActive(false);
            exitLevel.SetActive(false);
        }
        if (myGameType == 2)
        {
            arti.SetActive(true);
            exitLevel.SetActive(true);
        }
    }
}
