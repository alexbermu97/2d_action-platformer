﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ExitLevel : MonoBehaviour
{
    private GameObject player;
    [HideInInspector]
    public bool hasArtifact;
    private GameObject rewards;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        rewards = GameObject.Find("LevelRewards");
        hasArtifact = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (hasArtifact && collision.gameObject == player)
        {
            rewards.GetComponent<LevelRewards>().gameEnded = true;
            SceneManager.LoadScene("RewardsMenu");
        }
    }
}