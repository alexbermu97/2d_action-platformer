﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pause : MonoBehaviour
{
    [SerializeField]
    private GameObject joy;
    [SerializeField]
    private GameObject attackB;
    [SerializeField]
    private GameObject meleeB;
    [SerializeField]
    private GameObject jumpB;
    [SerializeField]
    private GameObject shieldB;
    [SerializeField]
    private GameObject pauseB;
    [SerializeField]
    private AudioSource pauseSound;
    [SerializeField]
    private AudioSource confirmSound;
    [SerializeField]
    private AudioSource denySound;

    [SerializeField]
    private GameObject background;

    void Start()
    {
        joy.SetActive(true);
        attackB.SetActive(true);
        meleeB.SetActive(true);
        jumpB.SetActive(true);
        shieldB.SetActive(true);
        pauseB.SetActive(true);

        background.SetActive(false);

        Time.timeScale = 1;
    }

    public void PauseGame()
    {
        pauseSound.Play();
        joy.SetActive(false);
        attackB.SetActive(false);
        meleeB.SetActive(false);
        jumpB.SetActive(false);
        shieldB.SetActive(false);
        pauseB.SetActive(false);

        background.SetActive(true);

        Time.timeScale = 0;
    }
    public void ContinueGame()
    {
        confirmSound.Play();
        joy.SetActive(true);
        attackB.SetActive(true);
        meleeB.SetActive(true);
        jumpB.SetActive(true);
        shieldB.SetActive(true);
        pauseB.SetActive(true);

        background.SetActive(false);

        Time.timeScale = 1;
    }
    public void ExitToMenu()
    {
        denySound.Play();
        SceneManager.LoadScene("LoadScreenPrep");
        Time.timeScale = 1;
    }
}