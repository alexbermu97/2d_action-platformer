﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Upgrades : MonoBehaviour
{
    private GameObject manager;

    public int cost;
    public GameObject confirmWindow;
    public GameObject closeMenu;
    public GameObject notEnoughMoney;
    public GameObject[] connectors;
    public GameObject[] buttons;
    public GameObject[] purchased;

    [SerializeField]
    private AudioSource confirmPSound;
    [SerializeField]
    private AudioSource denyPSound;
    [SerializeField]
    private AudioSource errorSound;

    private int upgradeCode;

    // Start is called before the first frame update
    void Start()
    {
        manager = GameObject.Find("GameManager");

        confirmWindow.SetActive(false);

        if (manager.GetComponent<UpgradesManager>().HPUpgrade1 != 0)
        {

            connectors[0].SetActive(true);
            connectors[2].SetActive(true);
            connectors[4].SetActive(true);
            connectors[5].SetActive(true);

            buttons[0].SetActive(true);
            buttons[4].SetActive(true);
            buttons[8].SetActive(true);
            buttons[13].SetActive(true);

            purchased[0].SetActive(true);
            PlayerPrefs.SetInt("HPUpgrade1", manager.GetComponent<UpgradesManager>().HPUpgrade1);
        }
        if (manager.GetComponent<UpgradesManager>().HPUpgrade2 != 0)
        {
            connectors[3].SetActive(true);
            buttons[1].SetActive(true);
            purchased[1].SetActive(true);
            PlayerPrefs.SetInt("HPUpgrade2", manager.GetComponent<UpgradesManager>().HPUpgrade2);
        }
        if (manager.GetComponent<UpgradesManager>().HPUpgrade3 != 0)
        {
            connectors[9].SetActive(true);
            buttons[2].SetActive(true);
            purchased[2].SetActive(true);
            PlayerPrefs.SetInt("HPUpgrade3", manager.GetComponent<UpgradesManager>().HPUpgrade3);
        }
        if (manager.GetComponent<UpgradesManager>().HPUpgrade4 != 0)
        {
            connectors[15].SetActive(true);
            buttons[3].SetActive(true);
            purchased[3].SetActive(true);
            PlayerPrefs.SetInt("HPUpgrade4", manager.GetComponent<UpgradesManager>().HPUpgrade4);
        }
        if (manager.GetComponent<UpgradesManager>().HPUpgrade5 != 0)
        {
            purchased[4].SetActive(true);
            PlayerPrefs.SetInt("HPUpgrade5", manager.GetComponent<UpgradesManager>().HPUpgrade5);
        }

        if (manager.GetComponent<UpgradesManager>().MPUpgrade1 != 0)
        {
            connectors[1].SetActive(true);
            buttons[5].SetActive(true);
            purchased[5].SetActive(true);
            PlayerPrefs.SetInt("MPUpgrade1", manager.GetComponent<UpgradesManager>().MPUpgrade1);
        }
        if (manager.GetComponent<UpgradesManager>().MPUpgrade2 != 0)
        {
            connectors[8].SetActive(true);
            buttons[6].SetActive(true);
            purchased[6].SetActive(true);
            PlayerPrefs.SetInt("MPUpgrade2", manager.GetComponent<UpgradesManager>().MPUpgrade2);
        }
        if (manager.GetComponent<UpgradesManager>().MPUpgrade3 != 0)
        {
            connectors[14].SetActive(true);
            buttons[7].SetActive(true);
            purchased[7].SetActive(true);
            PlayerPrefs.SetInt("MPUpgrade3", manager.GetComponent<UpgradesManager>().MPUpgrade3);
        }
        if (manager.GetComponent<UpgradesManager>().MPUpgrade4 != 0)
        {
            purchased[8].SetActive(true);
            PlayerPrefs.SetInt("MPUpgrade4", manager.GetComponent<UpgradesManager>().MPUpgrade4);
        }

        if (manager.GetComponent<UpgradesManager>().jetUpgrade1 != 0)
        {
            connectors[7].SetActive(true);
            buttons[9].SetActive(true);
            purchased[9].SetActive(true);
            PlayerPrefs.SetInt("jetUpgrade1", manager.GetComponent<UpgradesManager>().jetUpgrade1);
        }
        if (manager.GetComponent<UpgradesManager>().jetUpgrade2 != 0)
        {
            connectors[11].SetActive(true);
            buttons[10].SetActive(true);
            purchased[10].SetActive(true);
            PlayerPrefs.SetInt("jetUpgrade2", manager.GetComponent<UpgradesManager>().jetUpgrade2);
        }
        if (manager.GetComponent<UpgradesManager>().jetUpgrade3 != 0)
        {
            connectors[13].SetActive(true);
            buttons[11].SetActive(true);
            purchased[11].SetActive(true);
            PlayerPrefs.SetInt("jetUpgrade3", manager.GetComponent<UpgradesManager>().jetUpgrade3);
        }
        if (manager.GetComponent<UpgradesManager>().jetUpgrade4 != 0)
        {
            connectors[17].SetActive(true);
            buttons[12].SetActive(true);
            purchased[12].SetActive(true);
            PlayerPrefs.SetInt("jetUpgrade4", manager.GetComponent<UpgradesManager>().jetUpgrade4);
        }
        if (manager.GetComponent<UpgradesManager>().jetUpgrade5 != 0)
        {
            purchased[13].SetActive(true);
            PlayerPrefs.SetInt("jetUpgrade5", manager.GetComponent<UpgradesManager>().jetUpgrade5);
        }

        if (manager.GetComponent<UpgradesManager>().regenUpgrade1 != 0)
        {
            connectors[6].SetActive(true);
            buttons[14].SetActive(true);
            purchased[14].SetActive(true);
            PlayerPrefs.SetInt("regenUpgrade1", manager.GetComponent<UpgradesManager>().regenUpgrade1);
        }
        if (manager.GetComponent<UpgradesManager>().regenUpgrade2 != 0)
        {
            connectors[10].SetActive(true);
            buttons[15].SetActive(true);
            purchased[15].SetActive(true);
            PlayerPrefs.SetInt("regenUpgrade2", manager.GetComponent<UpgradesManager>().regenUpgrade2);
        }
        if (manager.GetComponent<UpgradesManager>().regenUpgrade3 != 0)
        {
            connectors[12].SetActive(true);
            buttons[16].SetActive(true);
            purchased[16].SetActive(true);
            PlayerPrefs.SetInt("regenUpgrade3", manager.GetComponent<UpgradesManager>().regenUpgrade3);
        }
        if (manager.GetComponent<UpgradesManager>().regenUpgrade4 != 0)
        {
            connectors[16].SetActive(true);
            buttons[17].SetActive(true);
            purchased[17].SetActive(true);
            PlayerPrefs.SetInt("regenUpgrade4", manager.GetComponent<UpgradesManager>().regenUpgrade4);
        }
        if (manager.GetComponent<UpgradesManager>().regenUpgrade5 != 0)
        {
            purchased[18].SetActive(true);
            PlayerPrefs.SetInt("regenUpgrade5", manager.GetComponent<UpgradesManager>().regenUpgrade5);
        }
    }

    //HP Upgrades
    public void PurchaseHPUpgrade1()
    {
        OpenConfirmWindow();
        upgradeCode = 1;
    }
    public void PurchaseHPUpgrade2()
    {
        OpenConfirmWindow();
        upgradeCode = 2;
    }
    public void PurchaseHPUpgrade3()
    {
        OpenConfirmWindow();
        upgradeCode = 3;
    }
    public void PurchaseHPUpgrade4()
    {
        OpenConfirmWindow();
        upgradeCode = 4;
    }
    public void PurchaseHPUpgrade5()
    {
        OpenConfirmWindow();
        upgradeCode = 5;
    }

    //MP Upgrades
    public void PurchaseMPUpgrade1()
    {
        OpenConfirmWindow();
        upgradeCode = 6;
    }
    public void PurchaseMPUpgrade2()
    {
        OpenConfirmWindow();
        upgradeCode = 7;
    }
    public void PurchaseMPUpgrade3()
    {
        OpenConfirmWindow();
        upgradeCode = 8;
    }
    public void PurchaseMPUpgrade4()
    {
        OpenConfirmWindow();
        upgradeCode = 9;
    }

    //Jet Upgrades
    public void PurchaseJetUpgrade1()
    {
        OpenConfirmWindow();
        upgradeCode = 10;
    }
    public void PurchaseJetUpgrade2()
    {
        OpenConfirmWindow();
        upgradeCode = 11;
    }
    public void PurchaseJetUpgrade3()
    {
        OpenConfirmWindow();
        upgradeCode = 12;
    }
    public void PurchaseJetUpgrade4()
    {
        OpenConfirmWindow();
        upgradeCode = 13;
    }
    public void PurchaseJetUpgrade5()
    {
        OpenConfirmWindow();
        upgradeCode = 14;
    }

    //Regen Upgrades
    public void PurchaseRegenUpgrade1()
    {
        OpenConfirmWindow();
        upgradeCode = 15;
    }
    public void PurchaseRegenUpgrade2()
    {
        OpenConfirmWindow();
        upgradeCode = 16;
    }
    public void PurchaseRegenUpgrade3()
    {
        OpenConfirmWindow();
        upgradeCode = 17;
    }
    public void PurchaseRegenUpgrade4()
    {
        OpenConfirmWindow();
        upgradeCode = 18;
    }
    public void PurchaseRegenUpgrade5()
    {
        OpenConfirmWindow();
        upgradeCode = 19;
    }

    void OpenConfirmWindow()
    {
        confirmWindow.SetActive(true);
        closeMenu.SetActive(false);
        notEnoughMoney.SetActive(false);
    }

    public void Confirm()
    {
        if (manager.GetComponent<MoneyManager>().myMoney >= cost)
        {
            if (upgradeCode == 1)
            {
                manager.GetComponent<UpgradesManager>().HPUpgrade1 = 1;

                connectors[0].SetActive(true);
                connectors[2].SetActive(true);
                connectors[4].SetActive(true);
                connectors[5].SetActive(true);

                buttons[0].SetActive(true);
                buttons[4].SetActive(true);
                buttons[8].SetActive(true);
                buttons[13].SetActive(true);

                purchased[0].SetActive(true);

                buttons[18].GetComponent<Button>().interactable = false;
            }
            else if (upgradeCode == 2)
            {
                manager.GetComponent<UpgradesManager>().HPUpgrade2 = 1;
                connectors[3].SetActive(true);
                buttons[1].SetActive(true);
                purchased[1].SetActive(true);

                buttons[0].GetComponent<Button>().interactable = false;
            }
            else if (upgradeCode == 3)
            {
                manager.GetComponent<UpgradesManager>().HPUpgrade3 = 1;
                connectors[9].SetActive(true);
                buttons[2].SetActive(true);
                purchased[2].SetActive(true);

                buttons[1].GetComponent<Button>().interactable = false;
            }
            else if (upgradeCode == 4)
            {
                manager.GetComponent<UpgradesManager>().HPUpgrade4 = 1;
                connectors[15].SetActive(true);
                buttons[3].SetActive(true);
                purchased[3].SetActive(true);

                buttons[2].GetComponent<Button>().interactable = false;
            }
            else if (upgradeCode == 5)
            {
                manager.GetComponent<UpgradesManager>().HPUpgrade5 = 1;
                purchased[4].SetActive(true);

                buttons[3].GetComponent<Button>().interactable = false;
            }
            else if (upgradeCode == 6)
            {
                manager.GetComponent<UpgradesManager>().MPUpgrade1 = 11;
                connectors[1].SetActive(true);
                buttons[5].SetActive(true);
                purchased[5].SetActive(true);

                buttons[4].GetComponent<Button>().interactable = false;
            }
            else if (upgradeCode == 7)
            {
                manager.GetComponent<UpgradesManager>().MPUpgrade2 = 1;
                connectors[8].SetActive(true);
                buttons[6].SetActive(true);
                purchased[6].SetActive(true);

                buttons[5].GetComponent<Button>().interactable = false;
            }
            else if (upgradeCode == 8)
            {
                manager.GetComponent<UpgradesManager>().MPUpgrade3 = 1;
                connectors[14].SetActive(true);
                buttons[7].SetActive(true);
                purchased[7].SetActive(true);

                buttons[6].GetComponent<Button>().interactable = false;
            }
            else if (upgradeCode == 9)
            {
                manager.GetComponent<UpgradesManager>().MPUpgrade4 = 1;
                purchased[8].SetActive(true);

                buttons[7].GetComponent<Button>().interactable = false;
            }
            else if (upgradeCode == 10)
            {
                manager.GetComponent<UpgradesManager>().jetUpgrade1 = 1;
                connectors[7].SetActive(true);
                buttons[9].SetActive(true);
                purchased[9].SetActive(true);

                buttons[8].GetComponent<Button>().interactable = false;
            }
            else if (upgradeCode == 11)
            {
                manager.GetComponent<UpgradesManager>().jetUpgrade2 = 1;
                connectors[11].SetActive(true);
                buttons[10].SetActive(true);
                purchased[10].SetActive(true);

                buttons[9].GetComponent<Button>().interactable = false;
            }
            else if (upgradeCode == 12)
            {
                manager.GetComponent<UpgradesManager>().jetUpgrade3 = 1;
                connectors[13].SetActive(true);
                buttons[11].SetActive(true);
                purchased[11].SetActive(true);

                buttons[10].GetComponent<Button>().interactable = false;
            }
            else if (upgradeCode == 13)
            {
                manager.GetComponent<UpgradesManager>().jetUpgrade4 = 1;
                connectors[17].SetActive(true);
                buttons[12].SetActive(true);
                purchased[12].SetActive(true);

                buttons[11].GetComponent<Button>().interactable = false;
            }
            else if (upgradeCode == 14)
            {
                manager.GetComponent<UpgradesManager>().jetUpgrade5 = 1;
                purchased[13].SetActive(true);

                buttons[12].GetComponent<Button>().interactable = false;
            }
            else if (upgradeCode == 15)
            {
                manager.GetComponent<UpgradesManager>().regenUpgrade1 = 1;
                connectors[6].SetActive(true);
                buttons[14].SetActive(true);
                purchased[14].SetActive(true);

                buttons[13].GetComponent<Button>().interactable = false;
            }
            else if (upgradeCode == 16)
            {
                manager.GetComponent<UpgradesManager>().regenUpgrade2 = 1;
                connectors[10].SetActive(true);
                buttons[15].SetActive(true);
                purchased[15].SetActive(true);

                buttons[14].GetComponent<Button>().interactable = false;
            }
            else if (upgradeCode == 17)
            {
                manager.GetComponent<UpgradesManager>().regenUpgrade3 = 1;
                connectors[12].SetActive(true);
                buttons[16].SetActive(true);
                purchased[16].SetActive(true);

                buttons[15].GetComponent<Button>().interactable = false;
            }
            else if (upgradeCode == 18)
            {
                manager.GetComponent<UpgradesManager>().regenUpgrade4 = 1;
                connectors[16].SetActive(true);
                buttons[17].SetActive(true);
                purchased[17].SetActive(true);

                buttons[16].GetComponent<Button>().interactable = false;
            }
            else if (upgradeCode == 19)
            {
                manager.GetComponent<UpgradesManager>().regenUpgrade5 = 1;
                purchased[18].SetActive(true);

                buttons[17].GetComponent<Button>().interactable = false;
            }

            manager.GetComponent<MoneyManager>().myMoney -= cost;
            confirmPSound.Play();
            confirmWindow.SetActive(false);
            closeMenu.SetActive(true);
            PlayerPrefs.SetInt("Money", manager.GetComponent<MoneyManager>().myMoney);
        }
        else
        {
            errorSound.Play();
            notEnoughMoney.SetActive(true);
        }
    }
    public void Deny()
    {
        denyPSound.Play();
        confirmWindow.SetActive(false);
        closeMenu.SetActive(true);
        notEnoughMoney.SetActive(false);
    }
}