﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoad : MonoBehaviour
{
    private GameObject manager;
    [SerializeField]
    private GameObject saveGame;

    void Start()
    {
        manager = GameObject.Find("GameManager");
        saveGame.GetComponent<SaveGame>().Save();
        StartCoroutine(LoadAsync());
    }

    IEnumerator LoadAsync()
    {
        AsyncOperation game = SceneManager.LoadSceneAsync(manager.GetComponent<GameWorld>().gameWorld);
        yield return new WaitForEndOfFrame();
    }
}