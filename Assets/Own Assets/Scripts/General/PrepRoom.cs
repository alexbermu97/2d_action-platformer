﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PrepRoom : MonoBehaviour
{
    private GameObject manager;

    [SerializeField]
    private GameObject joy;
    [SerializeField]
    private GameObject missionsButton;
    [SerializeField]
    private GameObject mTxt;
    [SerializeField]
    private GameObject missionBoard;
    [SerializeField]
    private GameObject worldsButton;
    [SerializeField]
    private GameObject wTxt;
    [SerializeField]
    private GameObject worldsBoard;
    [SerializeField]
    private GameObject launchButton;
    [SerializeField]
    private GameObject upgradeButton;
    [SerializeField]
    private GameObject upgradeBoard;

    [SerializeField]
    private GameObject bountyBorders;
    [SerializeField]
    private GameObject bounties;
    [SerializeField]
    private GameObject recoveryBorders;
    [SerializeField]
    private GameObject recoveries;
    [SerializeField]
    private GameObject questBorders;
    [SerializeField]
    private GameObject quests;

    [SerializeField]
    private GameObject menuButton;

    [SerializeField]
    private GameObject noMission;
    [SerializeField]
    private GameObject noWorld;
    [SerializeField]
    private GameObject noMissionOrWorld;

    [SerializeField]
    private GameObject saveGame;

    [SerializeField]
    private int tutorialMissionNumber;
    [SerializeField]
    private int world1Number;

    [SerializeField]
    private AudioSource confirmSound;
    [SerializeField]
    private AudioSource denySound;
    [SerializeField]
    private AudioSource errorSound;

    private bool gameSelected;
    private bool worldSelected;

    void Start()
    {
        manager = GameObject.Find("GameManager");
        missionBoard.SetActive(false);
        worldsBoard.SetActive(false);
        upgradeBoard.SetActive(false);
        gameSelected = false;
        worldSelected = false;

        noMission.SetActive(false);
        noWorld.SetActive(false);
        noMissionOrWorld.SetActive(false);
    }

    public void LaunchGame()
    {
        //Lanza la partida con los parámetros elegidos
        //No se puede lanzar si no se ha seleccionado una misión y un mundo
        if (gameSelected && worldSelected)
        {
            confirmSound.Play();
            saveGame.GetComponent<SaveGame>().Save();
            SceneManager.LoadScene("LoadScreen");
        }
        else if (!gameSelected && !worldSelected)
        {
            errorSound.Play();
            noMission.SetActive(false);
            noWorld.SetActive(false);
            noMissionOrWorld.SetActive(true);
        }
        else if (gameSelected && !worldSelected)
        {
            errorSound.Play();
            noMission.SetActive(false);
            noWorld.SetActive(true);
            noMissionOrWorld.SetActive(false);
        }
        else if (!gameSelected && worldSelected)
        {
            errorSound.Play();
            noMission.SetActive(true);
            noWorld.SetActive(false);
            noMissionOrWorld.SetActive(false);
        }
    }
    public void GameTypeScreen()
    {
        confirmSound.Play();
        //Abre un menú con las misiones
        joy.SetActive(false);
        missionsButton.SetActive(false);
        mTxt.SetActive(false);
        missionBoard.SetActive(true);

        bountyBorders.SetActive(false);
        bounties.SetActive(false);

        recoveryBorders.SetActive(true);
        recoveries.SetActive(true);

        questBorders.SetActive(false);
        quests.SetActive(false);

        noMission.SetActive(false);
        noWorld.SetActive(false);
        noMissionOrWorld.SetActive(false);
        menuButton.SetActive(false);
        //Al seleccionar una, se vuelve verdadero el booleano de selección de misión
        //Se actualiza la información en el manager
    }
    public void Bounties()
    {
        confirmSound.Play();
        //Se activan los componentes de Bounties y desactivan los de otros
        bountyBorders.SetActive(true);
        recoveryBorders.SetActive(false);
        questBorders.SetActive(false);

        bounties.SetActive(true);
        recoveries.SetActive(false);
        quests.SetActive(false);

        //Se asigna el tipo de misión en el Game Manager
        manager.GetComponent<GameType>().gameType = 1;
    }
    public void Recoveries()
    {
        confirmSound.Play();
        //Se activan los componentes de Recoveries y desactivan los de otros
        recoveryBorders.SetActive(true);
        bountyBorders.SetActive(false);
        questBorders.SetActive(false);

        recoveries.SetActive(true);
        bounties.SetActive(false);
        quests.SetActive(false);

        //Se asigna el tipo de misión en el Game Manager
        manager.GetComponent<GameType>().gameType = 2;
    }
    public void LowLevel()
    {
        if (manager.GetComponent<GameType>().gameType == 1)
            manager.GetComponent<LevelSizeManager>().levelSize = Random.Range(8, 12);
        if (manager.GetComponent<GameType>().gameType == 2)
            manager.GetComponent<LevelSizeManager>().levelSize = Random.Range(6, 10);

        manager.GetComponent<DifficultyManager>().difficulty = 1;
        gameSelected = true;
        confirmSound.Play();
        CloseGameTypeScreen();
    }
    public void MidLevel()
    {
        if (manager.GetComponent<GameType>().gameType == 1)
            manager.GetComponent<LevelSizeManager>().levelSize = Random.Range(16, 20);
        if (manager.GetComponent<GameType>().gameType == 2)
            manager.GetComponent<LevelSizeManager>().levelSize = Random.Range(14, 18);

        manager.GetComponent<DifficultyManager>().difficulty = 2;
        gameSelected = true;
        confirmSound.Play();
        CloseGameTypeScreen();
    }
    public void HighLevel()
    {
        if (manager.GetComponent<GameType>().gameType == 1)
            manager.GetComponent<LevelSizeManager>().levelSize = Random.Range(24, 28);
        if (manager.GetComponent<GameType>().gameType == 2)
            manager.GetComponent<LevelSizeManager>().levelSize = Random.Range(20, 24);

        manager.GetComponent<DifficultyManager>().difficulty = 3;
        gameSelected = true;
        confirmSound.Play();
        CloseGameTypeScreen();
    }
    public void Quests()
    {
        confirmSound.Play();
        //Se activan los componentes de Quests y desactivan los de otros
        questBorders.SetActive(true);
        recoveryBorders.SetActive(false);
        bountyBorders.SetActive(false);

        quests.SetActive(true);
        bounties.SetActive(false);
        recoveries.SetActive(false);
    }
    public void TutorialMission()
    {
        confirmSound.Play();
        manager.GetComponent<GameWorld>().gameWorld = tutorialMissionNumber;
        manager.GetComponent<GameType>().gameType = 1;
        SceneManager.LoadScene("LoadScreen");
    }
    public void CloseGameTypeScreen()
    {
        denySound.Play();
        joy.SetActive(true);
        missionsButton.SetActive(true);
        mTxt.SetActive(true);
        missionBoard.SetActive(false);
        menuButton.SetActive(true);
    }
    public void WorldSelectionScreen()
    {
        confirmSound.Play();
        //Abre un menú con los mundos
        joy.SetActive(false);
        worldsButton.SetActive(false);
        wTxt.SetActive(false);
        worldsBoard.SetActive(true);

        noMission.SetActive(false);
        noWorld.SetActive(false);
        noMissionOrWorld.SetActive(false);
        menuButton.SetActive(false);
        //Al seleccionar uno, se vuelve verdadero el booleano de selección de mundos
        //Se actualiza la información en el manager
    }
    public void World1()
    {
        confirmSound.Play();
        manager.GetComponent<GameWorld>().gameWorld = world1Number;
        worldSelected = true;
        CloseWorldsScreen();
    }
    public void CloseWorldsScreen()
    {
        denySound.Play();
        joy.SetActive(true);
        worldsButton.SetActive(true);
        wTxt.SetActive(true);
        worldsBoard.SetActive(false);
        menuButton.SetActive(true);
    }
    public void StoreScreen()
    {
        //Abre un menú con la tienda
    }
    public void UpgradeScreen()
    {
        confirmSound.Play();
        //Abre un menú con las mejoras
        upgradeButton.SetActive(false);
        joy.SetActive(false);
        menuButton.SetActive(false);

        upgradeBoard.SetActive(true);
    }
    public void CloseUpgradeScreen()
    {
        denySound.Play();
        upgradeBoard.SetActive(false);

        upgradeButton.SetActive(true);
        joy.SetActive(true);
        menuButton.SetActive(true);
    }
    public void CustomizationRoom()
    {
        //Lanza una escena aparte en la que se puede customizar al personaje
    }
    public void ExitToMenu()
    {
        denySound.Play();
        saveGame.GetComponent<SaveGame>().Save();
        SceneManager.LoadScene("MainMenu");
    }
}