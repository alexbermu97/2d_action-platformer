﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnNextLevelPiece : MonoBehaviour
{
    //Este script sirve para instanciar las piezas de nivel
    //La cantidad de piezas que se insatncian dependen de "levelSize"
    //Para elegir la pieza a instanciar se hace un random, que se guarda en "chosenMidPiece"
    [SerializeField]
    private GameObject[] midPieces;
    [SerializeField]
    private GameObject[] endPieces;

    private GameObject lvlManager;

    //private int chosenMidPiece;
    //private int chosenEndPiece;

    void Start()
    {
        lvlManager = GameObject.Find("GameManager");
        lvlManager.GetComponent<LevelSizeManager>().levelSize--;
        if (lvlManager.GetComponent<LevelSizeManager>().levelSize > 0)
        {
            //chosenMidPiece = Random.Range(0, midPieces.Length);
            Instantiate(midPieces[Random.Range(0, midPieces.Length)], gameObject.transform.position, gameObject.transform.rotation);
        }
        if (lvlManager.GetComponent<LevelSizeManager>().levelSize == 0)
        {
            //chosenEndPiece = Random.Range(0, endPieces.Length);
            Instantiate(endPieces[Random.Range(0, endPieces.Length)], gameObject.transform.position, gameObject.transform.rotation);
        }
    }
}
