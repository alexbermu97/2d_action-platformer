﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyManager : MonoBehaviour
{
    [Tooltip("Controla el dinero que tiene el jugador")]
    public int myMoney;
    [Tooltip("El dinero que el jugador obtiene durante el nivel")]
    public int levelMoney;
}