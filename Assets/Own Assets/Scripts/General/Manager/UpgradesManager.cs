﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradesManager : MonoBehaviour
{
    //Este código gestiona las mejoras, si están compradas o no

    //Salud
    public int HPUpgrade1;
    public int HPUpgrade2;
    public int HPUpgrade3;
    public int HPUpgrade4;
    public int HPUpgrade5;
    //Maná
    public int MPUpgrade1;
    public int MPUpgrade2;
    public int MPUpgrade3;
    public int MPUpgrade4;
    public int regenUpgrade1;
    public int regenUpgrade2;
    public int regenUpgrade3;
    public int regenUpgrade4;
    public int regenUpgrade5;
    //Salto
    public int jetUpgrade1;
    public int jetUpgrade2;
    public int jetUpgrade3;
    public int jetUpgrade4;
    public int jetUpgrade5;
}