﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameType : MonoBehaviour
{
    [Tooltip("Controla el modo de juego que se va a jugar. " +
        "1 = Bounties; 2 = Recoveries.")]
    public int gameType;
}
