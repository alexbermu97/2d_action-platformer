﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    [Tooltip("Controla los sonidos y música del juego.")]
    public int sound;
    public int music;
}