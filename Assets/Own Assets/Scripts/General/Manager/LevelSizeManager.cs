﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSizeManager : MonoBehaviour
{
    [Tooltip("Indica el número de bloques total del nivel (excluyendo piezas inicial y final). " +
        "Cuando se llega al último bloque se instancia la pieza final.")]
    public int levelSize; //Debe poder ser modificada por el script de selección de dificultad
}