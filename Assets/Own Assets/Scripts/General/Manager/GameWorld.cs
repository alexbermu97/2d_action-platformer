﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameWorld : MonoBehaviour
{
    [Tooltip("El mundo que se ha seleccionado. El int corresponde al número de la escena en el gestor de builds. " +
        "IMPORTANTE: Los mundos deben estar ordenados y de seguido, no puede haber otras escenas de por medio.")]
    public int gameWorld;
}
