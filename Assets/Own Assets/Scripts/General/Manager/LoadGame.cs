﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadGame : MonoBehaviour
{
    private GameObject manager;

    void Start()
    {
        manager = GameObject.Find("GameManager");

        //Load Tutorial State
        manager.GetComponent<TutorialManager>().firstTime = PlayerPrefs.GetInt("Tutorial");
        //Load Audio
        manager.GetComponent<SoundManager>().music = PlayerPrefs.GetInt("Music");
        manager.GetComponent<SoundManager>().sound = PlayerPrefs.GetInt("Sound");
        //Load Money
        manager.GetComponent<MoneyManager>().myMoney = PlayerPrefs.GetInt("Money");
        //Load HP Upgrades
        manager.GetComponent<UpgradesManager>().HPUpgrade1 = PlayerPrefs.GetInt("HPUpgrade1");
        manager.GetComponent<UpgradesManager>().HPUpgrade2 = PlayerPrefs.GetInt("HPUpgrade2");
        manager.GetComponent<UpgradesManager>().HPUpgrade3 = PlayerPrefs.GetInt("HPUpgrade3");
        manager.GetComponent<UpgradesManager>().HPUpgrade4 = PlayerPrefs.GetInt("HPUpgrade4");
        manager.GetComponent<UpgradesManager>().HPUpgrade5 = PlayerPrefs.GetInt("HPUpgrade5");
        //Load MP Upgrades
        manager.GetComponent<UpgradesManager>().MPUpgrade1 = PlayerPrefs.GetInt("MPUpgrade1");
        manager.GetComponent<UpgradesManager>().MPUpgrade2 = PlayerPrefs.GetInt("MPUpgrade2");
        manager.GetComponent<UpgradesManager>().MPUpgrade3 = PlayerPrefs.GetInt("MPUpgrade3");
        manager.GetComponent<UpgradesManager>().MPUpgrade4 = PlayerPrefs.GetInt("MPUpgrade4");
        manager.GetComponent<UpgradesManager>().regenUpgrade1 = PlayerPrefs.GetInt("regenUpgrade1");
        manager.GetComponent<UpgradesManager>().regenUpgrade2 = PlayerPrefs.GetInt("regenUpgrade2");
        manager.GetComponent<UpgradesManager>().regenUpgrade3 = PlayerPrefs.GetInt("regenUpgrade3");
        manager.GetComponent<UpgradesManager>().regenUpgrade4 = PlayerPrefs.GetInt("regenUpgrade4");
        manager.GetComponent<UpgradesManager>().regenUpgrade5 = PlayerPrefs.GetInt("regenUpgrade5");
        //Load Jet Upgrades
        manager.GetComponent<UpgradesManager>().jetUpgrade1 = PlayerPrefs.GetInt("jetUpgrade1");
        manager.GetComponent<UpgradesManager>().jetUpgrade2 = PlayerPrefs.GetInt("jetUpgrade2");
        manager.GetComponent<UpgradesManager>().jetUpgrade3 = PlayerPrefs.GetInt("jetUpgrade3");
        manager.GetComponent<UpgradesManager>().jetUpgrade4 = PlayerPrefs.GetInt("jetUpgrade4");
        manager.GetComponent<UpgradesManager>().jetUpgrade5 = PlayerPrefs.GetInt("jetUpgrade5");
    }
}