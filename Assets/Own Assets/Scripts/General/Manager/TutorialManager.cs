﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialManager : MonoBehaviour
{
    [Tooltip("Comprueba que sea la primera vez que el jugador juega para ejecutar el tutorial.")]
    public int firstTime;
}