﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveGame : MonoBehaviour
{
    private GameObject manager;

    public void Save()
    {
        manager = GameObject.Find("GameManager");
        //Save Tutorial State
        PlayerPrefs.SetInt("Tutorial", manager.GetComponent<TutorialManager>().firstTime);
        //Save Audio
        PlayerPrefs.SetInt("Music", manager.GetComponent<SoundManager>().music);
        PlayerPrefs.SetInt("Sound", manager.GetComponent<SoundManager>().sound);
        //Save Money
        PlayerPrefs.SetInt("Money", manager.GetComponent<MoneyManager>().myMoney);
        //Save HP Upgrades
        PlayerPrefs.SetInt("HPUpgrade1", manager.GetComponent<UpgradesManager>().HPUpgrade1);
        PlayerPrefs.SetInt("HPUpgrade2", manager.GetComponent<UpgradesManager>().HPUpgrade2);
        PlayerPrefs.SetInt("HPUpgrade3", manager.GetComponent<UpgradesManager>().HPUpgrade3);
        PlayerPrefs.SetInt("HPUpgrade4", manager.GetComponent<UpgradesManager>().HPUpgrade4);
        PlayerPrefs.SetInt("HPUpgrade5", manager.GetComponent<UpgradesManager>().HPUpgrade5);
        //Save MP Upgrades
        PlayerPrefs.SetInt("MPUpgrade1", manager.GetComponent<UpgradesManager>().MPUpgrade1);
        PlayerPrefs.SetInt("MPUpgrade2", manager.GetComponent<UpgradesManager>().MPUpgrade2);
        PlayerPrefs.SetInt("MPUpgrade3", manager.GetComponent<UpgradesManager>().MPUpgrade3);
        PlayerPrefs.SetInt("MPUpgrade4", manager.GetComponent<UpgradesManager>().MPUpgrade4);
        PlayerPrefs.SetInt("regenUpgrade1", manager.GetComponent<UpgradesManager>().regenUpgrade1);
        PlayerPrefs.SetInt("regenUpgrade2", manager.GetComponent<UpgradesManager>().regenUpgrade2);
        PlayerPrefs.SetInt("regenUpgrade3", manager.GetComponent<UpgradesManager>().regenUpgrade3);
        PlayerPrefs.SetInt("regenUpgrade4", manager.GetComponent<UpgradesManager>().regenUpgrade4);
        PlayerPrefs.SetInt("regenUpgrade5", manager.GetComponent<UpgradesManager>().regenUpgrade5);
        //Save Jet Upgrades
        PlayerPrefs.SetInt("jetUpgrade1", manager.GetComponent<UpgradesManager>().jetUpgrade1);
        PlayerPrefs.SetInt("jetUpgrade2", manager.GetComponent<UpgradesManager>().jetUpgrade2);
        PlayerPrefs.SetInt("jetUpgrade3", manager.GetComponent<UpgradesManager>().jetUpgrade3);
        PlayerPrefs.SetInt("jetUpgrade4", manager.GetComponent<UpgradesManager>().jetUpgrade4);
        PlayerPrefs.SetInt("jetUpgrade5", manager.GetComponent<UpgradesManager>().jetUpgrade5);
    }
}