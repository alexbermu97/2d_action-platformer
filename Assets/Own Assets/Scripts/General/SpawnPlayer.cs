﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class SpawnPlayer : MonoBehaviour
{
    [SerializeField]
    private GameObject player;
    [SerializeField]
    private CinemachineVirtualCamera cam;

    void Start()
    {
        Instantiate(player, gameObject.transform);
        cam.Follow = GameObject.FindGameObjectWithTag("Player").transform;
    }
}
