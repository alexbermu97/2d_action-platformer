﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundButtons : MonoBehaviour
{
    private GameObject manager;
    [SerializeField]
    private Image musicB;
    [SerializeField]
    private Image soundB;

    void Start()
    {
        manager = GameObject.Find("GameManager");

        if (manager.GetComponent<SoundManager>().music != 0)
            musicB.enabled = false;
        if (manager.GetComponent<SoundManager>().music == 0)
            musicB.enabled = true;

        if (manager.GetComponent<SoundManager>().sound != 0)
            soundB.enabled = false;
        if (manager.GetComponent<SoundManager>().sound == 0)
            soundB.enabled = true;
    }

    public void MusicButton()
    {
        if (manager.GetComponent<SoundManager>().music != 0)
        {
            manager.GetComponent<SoundManager>().music = 0;
            musicB.enabled = true;
        }
        else if (manager.GetComponent<SoundManager>().music == 0)
        {
            manager.GetComponent<SoundManager>().music = 1;
            musicB.enabled = false;
        }

        PlayerPrefs.SetInt("Music", manager.GetComponent<SoundManager>().music);
    }
    public void SoundButton()
    {
        if (manager.GetComponent<SoundManager>().sound !=0)
        {
            manager.GetComponent<SoundManager>().sound = 0;
            soundB.enabled = true;
        }
        else if (manager.GetComponent<SoundManager>().sound == 0)
        {
            manager.GetComponent<SoundManager>().sound = 1;
            soundB.enabled = false;
        }

        PlayerPrefs.SetInt("Sound", manager.GetComponent<SoundManager>().sound);
    }
}
