﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoadPrep : MonoBehaviour
{
    private GameObject manager;
    [SerializeField]
    private GameObject saveGame;

    void Start()
    {
        manager = GameObject.Find("GameManager");
        saveGame.GetComponent<SaveGame>().Save();
        StartCoroutine(LoadAsync());
    }

    IEnumerator LoadAsync()
    {
        if (manager.GetComponent<TutorialManager>().firstTime == 0)
        {
            AsyncOperation game = SceneManager.LoadSceneAsync("TutorialMission");
            yield return new WaitForEndOfFrame(); 
        }
        else if (manager.GetComponent<TutorialManager>().firstTime != 0)
        {
            AsyncOperation game = SceneManager.LoadSceneAsync("PrepScreen");
            yield return new WaitForEndOfFrame();
        }
    }
}