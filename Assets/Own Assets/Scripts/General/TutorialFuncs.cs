﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialFuncs : MonoBehaviour
{
    [SerializeField]
    private GameObject welcomePanel;
    [SerializeField]
    private GameObject intro1Button;
    [SerializeField]
    private GameObject intro2Button;
    [SerializeField]
    private GameObject joyIndicator;
    [SerializeField]
    private AudioSource confirmSound;

    void Start()
    {
        joyIndicator.SetActive(false);
        intro2Button.SetActive(false);
    }

    public void Intro1()
    {
        intro1Button.SetActive(false);
        intro2Button.SetActive(true);
        confirmSound.Play();
    }
    public void Intro2()
    {
        welcomePanel.SetActive(false);
        joyIndicator.SetActive(true);
        confirmSound.Play();
    }
    public void TeachJoystick()
    {
        joyIndicator.SetActive(false);
    }
}