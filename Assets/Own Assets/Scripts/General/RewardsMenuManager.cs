﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class RewardsMenuManager : MonoBehaviour
{
    private GameObject manager;
    [SerializeField]
    private TextMeshProUGUI counter;

    void Start()
    {
        manager = GameObject.Find("GameManager");
        counter.text = ("Obtained ") + manager.GetComponent<MoneyManager>().levelMoney.ToString() + (" Cr");
    }

    public void ReturnToMenu()
    {
        manager.GetComponent<MoneyManager>().myMoney += manager.GetComponent<MoneyManager>().levelMoney;
        SceneManager.LoadScene("LoadScreenPrep");
    }
}
