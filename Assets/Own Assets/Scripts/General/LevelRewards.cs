﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelRewards : MonoBehaviour
{
    private GameObject manager;
    public int droppedCredits;
    public int gameReward;
    public bool gameEnded;

    void Start()
    {
        manager = GameObject.Find("GameManager");
        manager.GetComponent<MoneyManager>().levelMoney = 0;
        if (manager.GetComponent<DifficultyManager>().difficulty == 1)
            gameReward = 1000;
        if (manager.GetComponent<DifficultyManager>().difficulty == 2)
            gameReward = 2000;
        if (manager.GetComponent<DifficultyManager>().difficulty == 3)
            gameReward = 3000;
    }

    void Update()
    {
        if (gameEnded)
        {
            GetRewards();
            gameEnded = false;
        }
    }

    void GetRewards()
    {
        manager.GetComponent<MoneyManager>().levelMoney = gameReward + droppedCredits;
    }
}
