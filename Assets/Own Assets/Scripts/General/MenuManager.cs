﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    [SerializeField]
    private GameObject quitButton;
    [SerializeField]
    private GameObject startButton;
    [SerializeField]
    private GameObject titleTxt;
    [SerializeField]
    private GameObject saveGame;
    [SerializeField]
    private GameObject fadeImage;
    [SerializeField]
    private GameObject startSound;
    [SerializeField]
    private float delay;

    private void Start()
    {
        fadeImage.SetActive(false);
    }

    public void QuitGame()
    {
        saveGame.GetComponent<SaveGame>().Save();
        Application.Quit();
    }
    public void StartGame()
    {
        StartCoroutine(SlightDelay());
    }
    IEnumerator SlightDelay()
    {
        //Para poder ejecutar el sonido y hacer un fade to black
        startSound.GetComponent<AudioSource>().Play();
        startButton.SetActive(false);
        yield return new WaitForSeconds(delay);
        fadeImage.SetActive(true);
        yield return new WaitForSeconds(delay);
        SceneManager.LoadScene("LoadScreenPrep");
    }
}