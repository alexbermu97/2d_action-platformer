﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrepRoomObj : MonoBehaviour
{
    private GameObject player;
    [SerializeField]
    private GameObject myButton;
    [SerializeField]
    private GameObject myTxt;

    void Start()
    {
        player = GameObject.Find("Player");
        myButton.SetActive(false);
        myTxt.SetActive(false);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == player)
        {
            myButton.SetActive(true);
            myTxt.SetActive(true);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject == player)
        {
            myButton.SetActive(false);
            myTxt.SetActive(false);
        }
    }
}