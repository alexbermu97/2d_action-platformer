﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayNight : MonoBehaviour
{
    [SerializeField]
    private GameObject day;
    [SerializeField]
    private GameObject dayProf;
    [SerializeField]
    private GameObject night;
    [SerializeField]
    private GameObject nightProf;
    private int dayOrNight;

    void Start()
    {
        dayOrNight = Random.Range(0, 2);
        if (dayOrNight == 0)
        {
            day.SetActive(true);
            dayProf.SetActive(true);
            night.SetActive(false);
            nightProf.SetActive(false);
        }
        else if (dayOrNight == 1)
        {
            day.SetActive(false);
            dayProf.SetActive(false);
            night.SetActive(true);
            nightProf.SetActive(true);
        }
    }
}
