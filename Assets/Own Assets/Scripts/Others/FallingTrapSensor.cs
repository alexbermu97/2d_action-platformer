﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingTrapSensor : MonoBehaviour
{
    private int cont = 0;
    public Rigidbody2D rb;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player" && cont == 0)
        {
            rb.gravityScale = 2f;
            cont++;
        }
    }
}