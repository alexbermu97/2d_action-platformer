﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditsDrop : MonoBehaviour
{
    private GameObject rewards;
    private Rigidbody2D rb;

    private bool burst;
    public int money;
    [SerializeField]
    private float magnitude;

    [SerializeField]
    private AudioSource SFX;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rewards = GameObject.Find("LevelRewards");
        money = Random.Range(10, 20);
        burst = true;
    }

    private void FixedUpdate()
    {
        if (burst)
        {
            rb.AddForce(Vector2.up * magnitude, ForceMode2D.Impulse);
            burst = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            SFX.Play();
            rewards.GetComponent<LevelRewards>().droppedCredits += money;
            GetComponent<SpriteRenderer>().enabled = false;
            GetComponent<CircleCollider2D>().enabled = false;
            rb.simulated = false;
            Destroy(gameObject, 1f);
        }
    }
}