﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RiskOfRain : MonoBehaviour
{
    [SerializeField]
    private GameObject lightRain;
    [SerializeField]
    private GameObject heavyRain;
    [SerializeField]
    private int makeItRain;

    void Start()
    {
        lightRain.SetActive(false);
        heavyRain.SetActive(false);
        makeItRain = Random.Range(0, 5);
        if (makeItRain == 0)
        {
            lightRain.SetActive(false);
            heavyRain.SetActive(false);
        }
        else if (makeItRain == 1 || makeItRain == 2)
        {
            lightRain.SetActive(true);
        }
        else
            heavyRain.SetActive(true);
    }
}
