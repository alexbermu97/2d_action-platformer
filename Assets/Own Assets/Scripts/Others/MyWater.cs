﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyWater : MonoBehaviour
{
    //Sonidos
    [SerializeField]
    private AudioSource[] waterSplashSFX;

    /*float[] xPos;
    float[] yPos;
    float[] velocities;
    float[] accels;
    LineRenderer body;

    GameObject[] meshObjs;
    Mesh[] meshes;

    GameObject[] colliders;

    float spring = 0.02f;
    float damp = 0.04f;
    float spread = 0.05f;
    float z = -1f;

    float baseheight;
    float left;
    float bottom;

    public GameObject splash;
    public Material mat;
    public GameObject watermesh;

    [SerializeField]
    float waterLeft;
    [SerializeField]
    float waterWidth;
    [SerializeField]
    float waterTop;
    [SerializeField]
    float waterBottom;

    [SerializeField]
    float mass;

    //Código de Alex Rose para la simulación de agua
    //Creando efectos dinámicos de agua en 2D en Unity, Alex Rose, 2014
    private void Start()
    {
        SpawnWater(waterLeft, waterWidth, waterTop, waterBottom);
    }
    public void SpawnWater(float _left, float _width, float _top, float _bottom)
    {
        int edgeCount = Mathf.RoundToInt(_width) * 5;
        int nodeCount = edgeCount + 1;

        body = gameObject.AddComponent<LineRenderer>();
        body.material = mat;
        body.material.renderQueue = 1000;
        body.positionCount = nodeCount;
        body.SetWidth(0.1f, 0.1f);

        xPos = new float[nodeCount];
        yPos = new float[nodeCount];
        velocities = new float[nodeCount];
        accels = new float[nodeCount];

        meshObjs = new GameObject[edgeCount];
        meshes = new Mesh[edgeCount];
        colliders = new GameObject[edgeCount];

        baseheight = _top;
        bottom = _bottom;
        left = _left;

        for (int i = 0; i < nodeCount; i++)
        {
            yPos[i] = _top;
            xPos[i] = _left + _width * i / edgeCount;
            accels[i] = 0;
            velocities[i] = 0;
            body.SetPosition(i, new Vector3(xPos[i], yPos[i], z));
        }

        for (int i = 0; i < edgeCount; i++)
        {
            meshes[i] = new Mesh();
            Vector3[] vertices = new Vector3[4];
            vertices[0] = new Vector3(xPos[i], yPos[i], z);
            vertices[1] = new Vector3(xPos[i + 1], yPos[i + 1], z);
            vertices[2] = new Vector3(xPos[i], bottom, z);
            vertices[3] = new Vector3(xPos[i + 1], bottom, z);

            Vector2[] uvs = new Vector2[4];
            uvs[0] = new Vector2(0, 1);
            uvs[1] = new Vector2(1, 1);
            uvs[2] = new Vector2(0, 0);
            uvs[3] = new Vector2(1, 0);

            int[] tris = new int[6] { 0, 1, 3, 3, 2, 0 };

            meshes[i].vertices = vertices;
            meshes[i].uv = uvs;
            meshes[i].triangles = tris;

            meshObjs[i] = Instantiate(watermesh, Vector3.zero, Quaternion.identity);
            meshObjs[i].GetComponent<MeshFilter>().mesh = meshes[i];
            meshObjs[i].transform.parent = transform;

            colliders[i] = new GameObject();
            colliders[i].name = "Trigger";
            colliders[i].AddComponent<BoxCollider2D>();
            colliders[i].transform.parent = transform;
            colliders[i].transform.position = new Vector3(_left + _width * (i + 0.5f) / edgeCount, _top - 0.5f, 0);
            colliders[i].GetComponent<BoxCollider2D>().isTrigger = true;
            colliders[i].AddComponent<WaterDetector>();
        }
    }

    void UpdateMeshes()
    {
        for (int i = 0; i < meshes.Length; i++)
        {
            Vector3[] vertices = new Vector3[4];
            vertices[0] = new Vector3(xPos[i], yPos[i], z);
            vertices[1] = new Vector3(xPos[i + 1], yPos[i + 1], z);
            vertices[2] = new Vector3(xPos[i], bottom, z);
            vertices[3] = new Vector3(xPos[i + 1], bottom, z);

            meshes[i].vertices = vertices;
        }
    }

    private void FixedUpdate()
    {
        for (int i = 0; i < xPos.Length; i++)
        {
            float force = spring * (yPos[i] - baseheight) + velocities[i] * damp;
            accels[i] = -force/mass;
            yPos[i] += velocities[i];
            velocities[i] += accels[i];
            body.SetPosition(i, new Vector3(xPos[i], yPos[i], z));
        }

        float[] leftDeltas = new float[xPos.Length];
        float[] rightDeltas = new float[xPos.Length];

        for (int j = 0; j < 8; j++)
        {
            for (int i = 0; i < xPos.Length; i++)
            {
                if (i > 0)
                {
                    leftDeltas[i] = spread * (yPos[i] - yPos[i - 1]);
                    velocities[i - 1] += leftDeltas[i];
                }
                if (i < xPos.Length - 1)
                {
                    rightDeltas[i] = spread * (yPos[i] - yPos[i + 1]);
                    velocities[i + 1] += rightDeltas[i];
                }
            }
        }
    }

    public void Splash(float _xpos, float velocity)
    {
        if (_xpos >= xPos[0] && _xpos <= xPos[xPos.Length - 1])
        {
            _xpos -= xPos[0];
            int index = Mathf.RoundToInt((xPos.Length - 1) * (_xpos / (xPos[xPos.Length - 1] - xPos[0])));
            velocities[index] = velocity;

            float lifetime = 0.93f + Mathf.Abs(velocity) * 0.07f;
            splash.GetComponent<ParticleSystem>().startSpeed = 8 + 2 * Mathf.Pow(Mathf.Abs(velocity), 0.5f);
            splash.GetComponent<ParticleSystem>().startSpeed = 9 + 2 * Mathf.Pow(Mathf.Abs(velocity), 0.5f);
            splash.GetComponent<ParticleSystem>().startLifetime = lifetime;

            GameObject splish = Instantiate(splash, transform.position, transform.rotation);
            Destroy(splish, lifetime + 0.3f);
        }
    }*/

    //Detección del jugador para reducir su velocidad de movimiento en agua
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<PlayerMovement>().inWater = true;
        }
        waterSplashSFX[Random.Range(0, 3)].Play();
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<PlayerMovement>().inWater = false;
        }
    }
}