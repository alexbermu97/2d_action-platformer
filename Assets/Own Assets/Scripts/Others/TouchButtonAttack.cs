﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TouchButtonAttack : MonoBehaviour, IPointerUpHandler, IPointerDownHandler
{
    [HideInInspector]
    public bool pressed;
    [HideInInspector]
    public bool longPressed;
    [HideInInspector]
    public bool tapped;
    [HideInInspector]
    public bool released;
    [HideInInspector]
    public bool winding;

    private float touchTime;
    [SerializeField]
    [Tooltip("Tiempo mínimo de pulsado del botón para iniciar segunda función.")]
    private float minTouchTime;

    void Update()
    {
        if (tapped)
        {
            touchTime += Time.deltaTime;
            if (touchTime >= minTouchTime/2f)
                winding = true;
            pressed = false;
            longPressed = false;
        }
        if (released)
        {
            if (touchTime > 0 && touchTime < minTouchTime)
                pressed = true;
            else if (touchTime >= minTouchTime)
                longPressed = true;
            released = false;
            winding = false;
            touchTime = 0;
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        tapped = true;
        released = false;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        tapped = false;
        released = true;
    }
}