﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthDrop : MonoBehaviour
{
    private GameObject player;
    private Rigidbody2D rb;

    private bool burst;
    [SerializeField]
    private float magnitude;

    [SerializeField]
    private AudioSource SFX;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        player = GameObject.FindGameObjectWithTag("Player");
        burst = true;
    }

    private void FixedUpdate()
    {
        if (burst)
        {
            rb.AddForce(Vector2.up * magnitude, ForceMode2D.Impulse);
            burst = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            SFX.Play();
            player.GetComponent<PlayerHealth>().currentHP++;
            GetComponent<SpriteRenderer>().enabled = false;
            GetComponent<CircleCollider2D>().enabled = false;
            rb.simulated = false;
            Destroy(gameObject, 1f);
        }
    }
}