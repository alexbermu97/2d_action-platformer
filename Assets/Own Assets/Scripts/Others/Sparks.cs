﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sparks : MonoBehaviour
{
    public GameObject parts;
    public float cont = 0;
    private float turnOff;
    [Range(0, 60)]
    public float minTime;
    [Range(0, 180)]
    public float maxTime;

    private void Start()
    {
        cont = Random.Range(minTime, maxTime);
        parts.SetActive(false);
    }

    void Update()
    {
        if (cont <= 0.1 && cont >= -0.1)
        {
            cont = Random.Range(minTime, maxTime);
            turnOff = 5;
            parts.SetActive(true);
        }
        if (cont != 0)
            cont -= Time.deltaTime;

        turnOff -= Time.deltaTime;
        if (turnOff <= 0)
        {
            turnOff = 0;
            parts.SetActive(false);
        }
    }
}