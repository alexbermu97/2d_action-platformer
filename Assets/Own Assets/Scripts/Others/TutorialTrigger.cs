﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialTrigger : MonoBehaviour
{
    [SerializeField]
    private GameObject tutorialControl;

    [SerializeField]
    private bool check;
    [SerializeField]
    private bool jump;
    [SerializeField]
    private bool jet;
    [SerializeField]
    private bool water;
    [SerializeField]
    private bool oneWay;
    [SerializeField]
    private bool oneWayDown;
    [SerializeField]
    private bool ice;
    [SerializeField]
    private bool shield;
    [SerializeField]
    private bool end;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (check)
                tutorialControl.GetComponent<TutorialControl>().Checkpoint();
            if (jump)
                tutorialControl.GetComponent<TutorialControl>().ActivateJumpButton();
            if (jet)
                tutorialControl.GetComponent<TutorialControl>().ActivateJetTutorial();
            if (water)
                tutorialControl.GetComponent<TutorialControl>().ActivateWaterTutorial();
            if (oneWay)
                tutorialControl.GetComponent<TutorialControl>().ActivateOneWayTutorial();
            if (oneWayDown)
                tutorialControl.GetComponent<TutorialControl>().ActivateOneWayDownTutorial();
            if (ice)
                tutorialControl.GetComponent<TutorialControl>().ActivateIceTutorial();
            if (shield)
                tutorialControl.GetComponent<TutorialControl>().ActivateShieldButton();
            if (end)
                tutorialControl.GetComponent<TutorialControl>().FinishTutorial();
        }
    }
}