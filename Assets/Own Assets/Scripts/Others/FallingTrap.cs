﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingTrap : MonoBehaviour
{
    [SerializeField]
    private int cont = 1;
    [SerializeField]
    private int healthDamage;
    [SerializeField]
    private GameObject player;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag != "LevelBound") 
        {
            if (collision.gameObject == player && cont == 1)
            {
                player.GetComponent<PlayerHealth>().currentHP -= healthDamage;
                player.GetComponent<PlayerHealth>().gotHit = true;
                cont++;
                Destroy(gameObject);
            }
            else if (collision.gameObject.tag == "Floor" || collision.gameObject.tag == "OneWayPlatform" || collision.gameObject.tag == "Ice")
            {
                Destroy(gameObject);
            }
        }
    }
}