﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyParent : MonoBehaviour
{
    //Health
    public float totalHP;
    [HideInInspector]
    public float currentHP;
    [HideInInspector]
    public bool hit;
    [HideInInspector]
    public bool isDead;
    public Collider2D coli1;
    public Collider2D coli2;

    //Movement
    [Range(0, 50f)]
    public float movSp;
    [HideInInspector]
    public bool lookLeft;
    public Rigidbody2D rb;

    //Patrol
    public Transform[] patrol;
    public float waitTime;
    public float startWaitTime;
    public int randomPos;

    //Attack
    public GameObject bullet;
    public Transform bulletSpawn;
    public float fireRate;
    [HideInInspector]
    public float nextShot;
    [HideInInspector]
    public RaycastHit2D rayHit;
    public bool isBoss;
    [HideInInspector]
    public Vector3 direction;

    //HUD
    public Slider healthBar;
    public Canvas canvas;

    //Player
    public GameObject player;
    public float playerRange; //Distancia a la que se detecta al jugador
    [HideInInspector]
    public bool playerDetected;
    public float playerDistance;
    public float playerFlee; //Distancia a partir de la cual el enemigo se aleja del jugador

    //Animations
    public Animator anim;

    //Death Particles
    public GameObject deathParticles;

    //Death Sound
    public AudioSource deathSFX;

    //Drop
    public GameObject[] drops; //Lista de posibles drops cuando el enemigo muere
    public int chosenDrop;

    public virtual void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        currentHP = totalHP;
        healthBar.value = totalHP / totalHP;
        lookLeft = true;
        deathParticles.SetActive(false);
        chosenDrop = Random.Range(0, drops.Length);
    }

    public virtual void Update()
    {
        playerDistance = Vector2.Distance(player.transform.position, transform.position); //Distancia a la que se encuentra el jugador
        rayHit = Physics2D.Raycast(bulletSpawn.position, player.transform.position);
        Debug.DrawLine(bulletSpawn.position, player.transform.position, Color.red);

        if (playerDistance <= playerRange)
            playerDetected = true;
        if (!isBoss)
        {
            if (playerDistance > playerRange + 10f)
                playerDetected = false;
        }

        if (playerDetected)
        {
            if (player.transform.position.x < transform.position.x && !lookLeft)
                TurnAround();
            if (player.transform.position.x > transform.position.x && lookLeft)
                TurnAround();
        }

        healthBar.value = currentHP / totalHP;
    }

    public virtual void Patrol()
    {
        //Los enemigos recorren una serie de puntos mientras no se detecte al jugador
    }

    public virtual void Attack()
    {
        //Los enemigos atacan con armas a distancia
    }

    public virtual void Flee()
    {
        //Los enemigos se alejan si el jugador está demasiado cerca
    }
    public virtual void TakeDamage(float hit)
    {
        currentHP -= hit;
        if (currentHP <= 0)
        {
            isDead = true;
            coli1.enabled = false;
            coli2.enabled = false;
            healthBar.enabled = false;
            currentHP = 0;
            player.GetComponent<PlayerShoot>().enemyInRange = false;
            Instantiate(drops[chosenDrop], transform.position, transform.rotation);
            deathSFX.Play();
            deathParticles.SetActive(true);
            GetComponent<SpriteRenderer>().enabled = false;
            Destroy(gameObject, 0.75f);
        }
        //Cuando recibe daño detecta al jugador
        playerDetected = true;
    }

    public void TurnAround()
    {
        lookLeft = !lookLeft;
        transform.Rotate(0f, 180f, 0f);
        canvas.transform.Rotate(0f, 180f, 0f);
    }
}