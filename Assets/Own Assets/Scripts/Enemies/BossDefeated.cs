﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BossDefeated : MonoBehaviour
{
    [SerializeField]
    private float waitTime;
    private GameObject rewards;
    private EnemyBoss boss;

    void Start()
    {
        rewards = GameObject.Find("LevelRewards");
        boss = GetComponent<EnemyBoss>();
    }

    void Update()
    {
        if (boss.bossDead)
            StartCoroutine(ReturnToMenu());
    }

    IEnumerator ReturnToMenu()
    {
        rewards.GetComponent<LevelRewards>().gameEnded = true;
        yield return new WaitForSeconds(waitTime);
        SceneManager.LoadScene("RewardsMenu");
    }
}