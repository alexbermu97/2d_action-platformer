﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnOffTelegraph : MonoBehaviour
{
    [SerializeField]
    private Animator anim;
    [SerializeField]
    private EnemyAir me;

    public void Fire()
    {
        me.shoot = true;
    }
    public void TurnOff()
    {
        anim.SetBool("PlayTelegraph", false);
    }
}