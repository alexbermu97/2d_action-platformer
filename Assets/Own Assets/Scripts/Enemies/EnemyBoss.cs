﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBoss : EnemyParent
{
    [HideInInspector]
    public bool bossDead;

    public override void Start()
    {
        base.Start();
        bossDead = false;
    }

    public override void Update()
    {
        base.Update();
        if (playerDetected)
            Patrol();
    }

    public override void TakeDamage(float hit)
    {
        currentHP -= hit;
        if (currentHP <= 0)
        {
            currentHP = 0;
            player.GetComponent<PlayerShoot>().enemyInRange = false;
            if (!bossDead)
                Instantiate(drops[chosenDrop], transform.position, transform.rotation);
            BossDefeated();
        }
    }
    public override void Patrol()
    {
        //La patrulla del jefe lo hace desplazarse por la sala durante el combate, mirando siempre al jugador
        base.Patrol();

        transform.position = Vector2.MoveTowards(transform.position, patrol[randomPos].position, movSp * Time.deltaTime);

        if (Vector2.Distance(transform.position, patrol[randomPos].position) < 0.2f)
        {
            if (waitTime <= 0)
            {
                randomPos = Random.Range(0, patrol.Length);
                waitTime = startWaitTime;
            }
            else
                waitTime -= Time.deltaTime;
        }
    }

    //Ataques del boss

    //Boss derrotado
    void BossDefeated()
    {
        bossDead = true;
        rb.gravityScale = 6f;
        playerDetected = false;
    }
}