﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAir : EnemyParent
{
    //Script para enemigos voladores
    public bool shoot = false;

    public override void Start()
    {
        base.Start();
        randomPos = Random.Range(0, patrol.Length);
    }
    public override void Update()
    {
        if (!isDead)
        {
            base.Update();
            if (!playerDetected)
            {
                anim.SetBool("PlayerDetected", false);
                Patrol();
            }
            if (playerDetected)
            {
                anim.SetBool("PlayerDetected", true);
                Attack();
                Flee();
            }
        }
    }

    public override void Patrol()
    {
        base.Patrol();
        if (patrol[randomPos].position.x < transform.position.x && !lookLeft)
            TurnAround();
        if (patrol[randomPos].position.x > transform.position.x && lookLeft)
            TurnAround();

        transform.position = Vector2.MoveTowards(transform.position, patrol[randomPos].position, movSp * Time.deltaTime);

        if (Vector2.Distance(transform.position, patrol[randomPos].position) < 0.2f)
        {
            if (waitTime <= 0)
            {
                randomPos = Random.Range(0, patrol.Length);
                waitTime = startWaitTime;
            }
            else
                waitTime -= Time.deltaTime;
        }
    }

    public override void Attack()
    {
        base.Attack(); 
        if (rayHit.collider != null)
        {
            direction = (player.transform.position - bulletSpawn.position).normalized;
            if (nextShot < Time.time)
            {
                bulletSpawn.GetComponent<Animator>().SetBool("PlayTelegraph", true);
                nextShot = fireRate + Time.time;
            }
            if (shoot)
            {
                float rotation = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
                Instantiate(bullet, bulletSpawn.position, Quaternion.Euler(0f, 0f, rotation));
                shoot = false;
            }
        }
    }

    public override void Flee()
    {
        base.Flee();
        if (playerDistance <= playerFlee)
        {
            if (lookLeft)
            {
                Vector3 dir = transform.position - player.transform.position;
                transform.Translate(dir.normalized * (movSp / 2) * Time.deltaTime);
            }
            if (!lookLeft)
            {
                Vector3 dir = player.transform.position - transform.position;
                transform.Translate(dir.normalized * (movSp / 2) * Time.deltaTime);
            }
        }
    }
}